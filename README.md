Examples from classes

Lectures(video): 
https://www.youtube.com/playlist?list=PLwhHMiWOwUBqbhjKgl-QXYt2Nhk-rPRow

Homework:
https://www.hackerrank.com/javacorefebruary2020

Книги:
1) Effective Java: Joshua Bloch
2) Философия Java - Брюс Эккель
3) Алгоритмы на Java - Роберт Седжвик
4) Design patterns - GoF

Presentations:
https://drive.google.com/drive/folders/17L8Rw2MqJMbqJ9KMy4DfeEuzwkyjQ29O

Exam preparation book:
OCA/OCP Practice Tests Exam 1Z0 808 and Exam 1Z (Scott Selikoff and Jeanne Boyarsky)
https://www.academia.edu/33102355/OCA_OCP_Practice_Tests_Exam_1Z0_808_and_Exam_1Z
https://github.com/arjspe/boekjes/blob/master/OCA%20OCP%20Java%20SE%208%20Programmer%20Practice%20Tests.pdf

Generics, PECS and Extends/Super
https://stackoverflow.com/questions/4343202/difference-between-super-t-and-extends-t-in-java

LiveLock
https://www.logicbig.com/tutorials/core-java-tutorial/java-multi-threading/thread-livelock.html

Hierarchy:
http://www.falkhausen.de/Java-8/java.util/Collection-Hierarchy.html

Algorithms:
https://visualgo.net/en

Courses:
https://www.coursera.org/learn/analysis-of-algorithms
https://www.coursera.org/learn/algorithms-part1?=
https://www.coursera.org/learn/algorithms-part2?=

https://algs4.cs.princeton.edu/cheatsheet/

Assertions:
https://docs.oracle.com/javase/8/docs/technotes/guides/language/assert.html