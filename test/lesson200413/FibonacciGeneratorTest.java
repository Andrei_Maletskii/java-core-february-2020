package lesson200413;

import lesson200305.Fibonacci;
import org.junit.jupiter.api.*;

import java.math.BigInteger;

// TDD Test Driven Development
// 1. Write Test
// 2. Launch tests
// 3.1 If PASS - keep writing tests
// 3.2 If FAILED - made smallest changes in the code to make test pass
// 4 Repeat until coverage will satisfy us

// 1 - Unit Testing
// 2 - Integration testing
// 3 - System testing
// 4 - E2E - End to End testing
// 5 - Smoke testing, Canary deployment...etc
class FibonacciGeneratorTest {

    private static FibonacciGenerator fibonacciGenerator;

    @BeforeAll
    static void staticSetUp() {
        System.out.println("Creating Fibonacci Generator...");
        fibonacciGenerator = new FibonacciGenerator();
    }

    @BeforeEach
    void setUp(TestInfo testInfo) {
        System.out.println("Executing test " + testInfo.getDisplayName());
        fibonacciGenerator.clearCache();
    }

    @AfterEach
    void tearDown(TestInfo testInfo) {
        System.out.println("Test " + testInfo.getDisplayName() + " executed");
    }

    @AfterAll
    static void staticTearDown() {
        System.out.println("All tests executed");
    }

    @Test
    void shouldReturnZeroForZero() {
        BigInteger result = fibonacciGenerator.fibFast(0);

        Assertions.assertEquals(BigInteger.ZERO, result);
    }

    @Test
    void shouldReturnOneForOne() {
        BigInteger result = fibonacciGenerator.fibFast(1);

        Assertions.assertEquals(BigInteger.ONE, result);
    }

    @Test
    void shouldReturnOneForTwo() {
        BigInteger result = fibonacciGenerator.fibFast(2);

        Assertions.assertEquals(BigInteger.ONE, result);
    }

    @Test
    void shouldReturn55For10() {
        BigInteger result = fibonacciGenerator.fibFast(10);

        Assertions.assertEquals(BigInteger.valueOf(55), result);
    }

    @Test
    void shouldThrowAnIllegalArgumentExceptionForNegativeArg() {
        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> fibonacciGenerator.fibFast(-1)
        );
    }

}