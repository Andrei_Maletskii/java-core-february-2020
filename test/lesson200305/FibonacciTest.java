package lesson200305;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.*;

class FibonacciTest {

    @Test
    void shouldReturnOneForOne() {
        BigInteger result = Fibonacci.fibFast(1);

//        assert result.equals(BigInteger.ONE);
        Assertions.assertEquals(BigInteger.ONE, result);
    }

    @Test
    void shouldReturnZeroForZero() {
        BigInteger result = Fibonacci.fibFast(0);

        Assertions.assertEquals(BigInteger.ZERO, result);
    }

    @Test
    void shouldReturnOneForTwo() {
        BigInteger result = Fibonacci.fibFast(2);

        Assertions.assertEquals(BigInteger.ONE, result);
    }

    @Test
    void shouldReturn55For10() {
        BigInteger result = Fibonacci.fibFast(10);

        Assertions.assertEquals(BigInteger.valueOf(55), result);
    }

    @Test
    void shouldThrowAnIllegalArgumentExceptionForNegativeArg() {
        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> Fibonacci.fibFast(-1)
        );
    }
}