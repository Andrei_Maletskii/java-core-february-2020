package lesson200330;

public class MainDeath {

    public static void main(String[] args) {
        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 5; i++) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("Working " + i);
                }
            }
        });

        thread1.start();

//        throw new RuntimeException(); // no matter
    }
}
