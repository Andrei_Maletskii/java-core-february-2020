package lesson200330;

public class ExceptionHandling {

    public static void main(String[] args) {
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                System.out.println("Default handler " + t.getName() + " got " + e);
            }
        });

        Thread thread1 = new Thread(new ExceptionalRunnable(1));

        Thread thread2 = new Thread(new ExceptionalRunnable(2));

        thread2.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                System.out.println("Special handling for " + t.getName() + " got " + e);
            }
        });

        ThreadGroup threadGroup = new ThreadGroup("root") {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                System.out.println("Group handling for " + t.getName() + " got " + e);
            }
        };

        Thread thread3 = new Thread(threadGroup, new ExceptionalRunnable(3));
        thread3.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                System.out.println("Special handling for " + t.getName() + " got " + e);
            }
        });

        thread1.start();
        thread2.start();
        thread3.start();

        // handling
        // 1 - thread handler
        // 2 - group handler
        // 3 - default handler

    }
}

class ExceptionalRunnable implements Runnable {
    private int id;

    public ExceptionalRunnable(int id) {
        this.id = id;
    }

    @Override
    public void run() {
        throw new RuntimeException("Exception" + id);
    }
}