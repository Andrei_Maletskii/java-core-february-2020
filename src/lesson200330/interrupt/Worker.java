package lesson200330.interrupt;

public class Worker implements Runnable {

    private int count = 0;

    @Override
    public void run() {
        while (!Thread.interrupted()) {
            System.out.println(
                    "Working..." + count++
            );
        }

//        while (true) { // no affect if interrupted
//            System.out.println(
//                    "Working..." + count++
//            );
//        }
    }
}
