package lesson200330.interrupt;

public class InterruptExample {

    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(new Worker());

        thread.start();

        System.out.println(thread.isInterrupted());

        thread.interrupt();

        for (int i = 0; i < 10; i++) {
            Thread.sleep(1000);
            System.out.println(thread.isInterrupted());
        }
    }
}
