package lesson200330;

public class ThreadYieldExample {

    public static void main(String[] args) {
        Talking talking = new Talking();
        Walking walking = new Walking();

        talking.start();
        walking.start();
    }
}

class Talking extends Thread {

    @Override
    public void run() {
        while (true) {
            Thread.yield();
            System.out.println("Talking...");
        }
    }
}

class Walking extends Thread {

    @Override
    public void run() {
        while (true) {
            System.out.println("Walking...");
        }
    }
}
