package lesson200330.priority;

public class PriorityExample {

    public static void main(String[] args) {
        Thread talking = new Thread(new TalkingThread());
        Thread walking = new Thread(new WalkingThread());

        talking.setName("Thread to talk");
        talking.setPriority(Thread.MAX_PRIORITY);
        walking.setName("Thread to walk");
        walking.setPriority(Thread.MIN_PRIORITY);

        talking.start();
        walking.start();

    }
}
