package lesson200330;

public class JoinExample {

    public volatile static int sum = 0;

    public static void main(String[] args) {
        System.out.println("Main started");
        Thread myThread = new Thread(new Calculator(3, 5));

        myThread.start();

//        try {
//            myThread.join(5000, 5000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

        System.out.println("myThread finished!, result = " + sum);
    }
}

class Calculator implements Runnable {

    private int a;
    private int b;

    public Calculator(int a, int b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public void run() {
        JoinExample.sum = a + b;
    }
}
