package lesson200330;

import java.util.Arrays;

public class ThreadGroupExample {

    public static void main(String[] args) throws InterruptedException {
        ThreadGroup root = new ThreadGroup("root") {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                System.out.println(t.getName() + " thread got " + e.getMessage());
            }
        };

        ThreadGroup workers1 = new ThreadGroup(root, "workers1") {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                System.out.println(t.getName() + " THREAD GOT " + e.getMessage());
            }
        };
        ThreadGroup workers2 = new ThreadGroup(root, "workers2");



        Thread thread11 = new Thread(workers1, new Worker(), "worker11");
        Thread thread12 = new Thread(workers1, new Worker(), "worker12");
        Thread thread21 = new Thread(workers2, new Worker(), "worker21");
        Thread thread22 = new Thread(workers2, new Worker(), "worker22");

        thread21.setDaemon(true);
        thread22.setDaemon(true);

        thread11.start();
        thread12.start();
        thread21.start();
        thread22.start();


        Thread.sleep(2000);

        Thread[] threads = new Thread[root.activeCount()];
        root.enumerate(threads, true);

        ThreadGroup[] threadGroups = new ThreadGroup[root.activeGroupCount()];
        root.enumerate(threadGroups);

        System.out.println(Arrays.toString(threads));
        System.out.println(Arrays.toString(threadGroups));

        workers1.interrupt();

        Thread.sleep(1000);

//        root.destroy();

        for (ThreadGroup threadGroup : threadGroups) {
            System.out.println(threadGroup.getName() + " destroyed " + threadGroup.isDestroyed());
        }

    }
}

class Worker implements Runnable {

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException("Was interrupted", e);
            }
        }
    }
}
