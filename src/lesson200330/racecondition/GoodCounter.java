package lesson200330.racecondition;

import java.util.concurrent.atomic.AtomicInteger;

public class GoodCounter implements Counter {
    private AtomicInteger integer = new AtomicInteger(0);

    @Override
    public int getCount() {
        return integer.get();
    }

    @Override
    public void increment() {
        // i = new AtomicInteger(0);
        // 1
        int result1 = integer.incrementAndGet(); // ++i
        // 0
        int result2 = integer.getAndIncrement(); // i++
        // 5
        int result3 = integer.addAndGet(5);
        // 0
        int result4 = integer.getAndAdd(5);

    }
}
