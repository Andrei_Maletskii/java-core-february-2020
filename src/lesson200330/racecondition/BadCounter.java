package lesson200330.racecondition;

public class BadCounter implements Counter {
    private int i = 0;
    private Object lock = new Object();

    @Override
    public int getCount() {
        return i;
    }

    @Override
    public synchronized void increment() { // synchronized method can solve it
//        synchronized (this) { // the same as synchronized method
//            i++;
//        }

        i++;

        // thread1
        // read i       // thread2
                        // read i
        // i + 1 = 1
                        // i + 1 = 1
        // write i
                        // write i

        // i = 1


        // long or double read
        // read first 32 bits
        // read second 32 bits

        // long or double write
        // write first 32 bits
        // write second 32 bits

    }

    public void increment1() { // synchronized block can solve it
        synchronized (lock) {
            i++;
        }

        // thread1
        // read i       // thread2
        // read i
        // i + 1 = 1
        // i + 1 = 1
        // write i
        // write i

        // i = 1
    }

    public synchronized static void staticSyncMethod() {
        // operators
    }

    public static void staticMethodWithSyncBlock() {
        synchronized (BadCounter.class) {
            // operators
        }
    }


}
