package lesson200330.racecondition;

public interface Counter {

    int getCount();

    void increment();
}
