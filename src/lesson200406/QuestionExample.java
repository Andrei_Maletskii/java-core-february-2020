package lesson200406;

import java.util.Random;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class QuestionExample {

    public static void main(String[] args) {
        Predicate pred = c -> c.equals("hello");

        System.out.println(pred.test("hello"));

        testString("hello", s -> s.length() == 5);

        Random random = new Random();
        Stream<Integer> stream = Stream.generate(() -> random.nextInt());
        Stream<Integer> stream1 = Stream.generate(random::nextInt);
    }

    public static boolean testString(String str, Predicate<String> predicate) {
        return predicate.test(str);
    }
}
