package lesson200406;

import java.nio.file.DirectoryStream;
import java.util.function.Predicate;

public class PredicateExample {

    public static void main(String[] args) {
        Predicate<String> predicate = new Predicate<>() {
            @Override
            public boolean test(String s) {
                return s.isEmpty();
            }
        };

        Predicate<String> predicate1 = (String s) -> {
            return s.isEmpty();
        };

        Predicate<String> predicate2 = (s) -> {
            return s.isEmpty();
        };

        Predicate<String> predicate3 = s -> {
            return s.isEmpty();
        };

        Predicate<String> predicate4 = s -> s.isEmpty();

        Predicate<String> predicate5 = String::isEmpty;

    }
}
