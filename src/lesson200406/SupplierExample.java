package lesson200406;

import java.util.function.Supplier;

public class SupplierExample {

    public static void main(String[] args) {
        Supplier<String> supplier = new Supplier<>() {
            @Override
            public String get() {
                return "hello";
            }
        };

        Supplier<String> supplier1 = () -> {
            return "hello";
        };

        Supplier<String> supplier2 = () -> "hello";
    }
}
