package lesson200406;

import java.util.function.Function;

public class FunctionExample {

    public static void main(String[] args) {
        Function<String, Integer> function = new Function<String, Integer>() {
            @Override
            public Integer apply(String s) {
                return s.length();
            }
        };

        Function<String, Integer> function1 = (String s) -> {
            return s.length();
        };

        Function<String, Integer> function2 = s -> {
            return s.length();
        };

        Function<String, Integer> function3 = s -> s.length();

        Function<String, Integer> function4 = String::length;
    }
}
