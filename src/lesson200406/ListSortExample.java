package lesson200406;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class ListSortExample {

    public static void main(String[] args) {
        List<String> list = Arrays.asList(
                "str2",
                "str3254125",
                "str32",
                "str",
                "str3254"
        );

//        Collections.sort(list);

        list.sort(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.length() - o2.length();
            }
        });

        list.sort((str1, str2) -> {
            return str1.length() - str2.length();
        });

        MyFuncInt myFuncInt = () -> {
            return 1;
        };


    }
}

//@FunctionalInterface
interface MyFuncInt {
    // func descriptor void -> int
    int method1();
    default int method2() {
        return 0;
    }
}
