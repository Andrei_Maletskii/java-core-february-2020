package lesson200406;

import java.io.FilenameFilter;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.*;

public class MethodReferenceExample {

    public static void main(String[] args) {
        // 1 type
        // ([args]) -> ClassName.staticMethod([args]), [] - optional
        // () -> ClassName.staticMethod()
        // ClassName::staticMethod

        Supplier<Thread> supplier = () -> Thread.currentThread();
        Supplier<Thread> supplier1 = Thread::currentThread;

        Function<String, List<String>> stringListFunction = (String s) -> List.of(s);
        Function<String, List<String>> stringListFunction1 = List::of;
        Function<String, List<String>> stringListFunction3 = (String s) -> {
            System.out.println("hello");
            return List.of(s);
        };

        BiFunction<String, String, List<String>> biFunction = (str1, str2) -> List.of(str1, str2);
        BiFunction<String, String, List<String>> biFunction1 = List::of;
        Function<String[], List<String>> func = List::of;
        
        // 2 type
        // (arg, [rest...]) -> arg.instanceMethod([rest])
        // (arg) -> arg.insanceMethod()
        // ClassName::instanceMethod

        ArrayList<String> arrayList = new ArrayList<>();
        BiFunction<ArrayList<String>, String, Boolean> biFunc = (list, str) -> list.add(str);
        BiFunction<ArrayList<String>, String, Boolean> biFunc2 = (list, str) -> {
            return list.add(str);
        };

        BiConsumer<ArrayList<String>, String> biConsumer = (list, str) -> {
            list.add(str);
        };

        BiFunction<ArrayList<String>, String, Boolean> biFunc1 = ArrayList::add;
        BiConsumer<ArrayList<String>, String> biConsumer1 = ArrayList::add;

        Comparor<String> comparor = (comparator, a, b) -> comparator.compare(a, b);
        Comparor<String> comparor1 = Comparator::compare;

        // 3 type
        // (args) -> expr.instanceMethod(args), expr - i.e. System.out, getString()
        // expr::instanceMethod

        Consumer<String> consumer = obj -> {
            PrintStream out = System.out;
            out.println(obj);
        };

        Consumer<String> consumer1 = obj -> {
            System.out.println(obj);
        };

        Consumer<String> consumer2 = System.out::println;

        Predicate<String> predicate = (prefix) -> getString().startsWith(prefix);

        predicate.test("he");

        Predicate<String> predicate1 = MethodReferenceExample.getString()::startsWith;

        // 4 type
        // ([args]) -> new ClassName(args)
        // () -> new ClassName()
        // ClassName::new

        Supplier<ArrayList<String>> supplier3 = () -> new ArrayList<>();
        Supplier<ArrayList<String>> supplier4 = ArrayList::new;

        Function<byte[], String> byteArrayToString = byteArray -> new String(byteArray);
        Function<byte[], String> byteArrayToString1 = String::new;
    }

    public static String getString() {
        return "hello";
    }
}

// (Comparator<T>, T, T) -> int
@FunctionalInterface
interface Comparor<T> {
    int compareWithComparator(Comparator<T> comparator, T obj1, T obj2);
}
