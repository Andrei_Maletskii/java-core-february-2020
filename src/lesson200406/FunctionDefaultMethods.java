package lesson200406;

import java.util.Objects;
import java.util.function.Function;

public class FunctionDefaultMethods {

    public static void main(String[] args) {
        Function<Integer, String> function = Objects::toString;
        Function<String, Double> function1 = (str) -> {
            return (double) str.length();
        };

        Function<Integer, Double> integerDoubleFunction = function.andThen(function1);

    }
}
