package lesson200406.person;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ComparePerson {

    public static void main(String[] args) {
        List<Person> personList = new ArrayList<>();

        Computer computer = new Computer(null);
        personList.add(new Person(computer, "Amy", 46));
        personList.add(new Person(computer, "Amy", 26));
        personList.add(new Person(computer, "Ivan", 27));
        personList.add(new Person(computer, "Vladimir", 44));
        personList.add(new Person(computer, "Fedor", 18));

        personList.sort((p1, p2) -> {
            int nameCompareResult = p1.getName().compareTo(p2.getName());

            if (nameCompareResult != 0) {
                return nameCompareResult;
            }

            return Integer.compare(p1.getAge(), p2.getAge());
        });

        personList.sort(
                Comparator.comparing(Person::getName) //.reversed()
                        .thenComparingInt(Person::getAge)
        );

        personList.sort(
                Comparator.comparing(Person::getName)
                        .thenComparingInt(Person::getAge)
                        .thenComparing(
                                Person::getComputer,
                                Comparator.comparing(
                                        Computer::getProcessor,
                                        Comparator.comparing(Processor::getName)
                                )
                        )
        );
    }
}
