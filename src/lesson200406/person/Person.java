package lesson200406.person;

public class Person {

    private Computer computer;
    private String name;
    private int age;

    public Person(Computer computer, String name, int age) {
        this.computer = computer;
        this.name = name;
        this.age = age;
    }

    public Computer getComputer() {
        return computer;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "computer=" + computer +
                ", name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
