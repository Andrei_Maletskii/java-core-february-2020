package lesson200406.person;

public class Computer {

    private Processor processor;

    public Computer(Processor processor) {
        this.processor = processor;
    }

    public Processor getProcessor() {
        return processor;
    }
}
