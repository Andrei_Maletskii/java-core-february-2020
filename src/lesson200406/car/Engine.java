package lesson200406.car;

public class Engine {

    private Oil oil;

    public Engine(Oil oil) {
        this.oil = oil;
    }

    public Engine() {
    }

    public Oil getOil() {
        return oil;
    }
}
