package lesson200406.car;

import java.util.Optional;

public class CarUse {

    public static void main(String[] args) {
//        driveCarOldStyle(null);
//        driveCarOldStyle(new Car());
//        driveCarOldStyle(new Car(new Engine()));
//        driveCarOldStyle(new Car(new Engine(new Oil())));
//        driveCarOldStyle(new Car(new Engine(new Oil("Motul"))));

//        driveCarNewStyle(null);
//        driveCarNewStyle(new Car());
//        driveCarNewStyle(new Car(new Engine()));
//        driveCarNewStyle(new Car(new Engine(new Oil())));
//        driveCarNewStyle(new Car(new Engine(new Oil("Motul"))));
//        Optional<Car> optionalCar = getOptionalCar(0);

        System.out.println(whatIsYourCarOil(null));
        System.out.println(whatIsYourCarOil(new Car()));
        System.out.println(whatIsYourCarOil(new Car(new Engine())));
        System.out.println(whatIsYourCarOil(new Car(new Engine(new Oil()))));
        System.out.println(whatIsYourCarOil(new Car(new Engine(new Oil("Motul")))));

        Optional<Car> optionalCar = Optional.ofNullable(null);

        Optional<Car> optionalCar2 = optionalCar.or(() -> Optional.of(new Car()));

        if (optionalCar2.isPresent()) {
            Car car1 = optionalCar2.get();
        }

        if (optionalCar2.isEmpty()) {
            System.out.println("empty");
        }

        optionalCar2.ifPresent(car -> {
            System.out.println(car);
        });

        Optional<Car> car = Optional.of(new Car());

        Optional<Car> car1 = car.filter(c -> c.getEngine() != null);
        System.out.println(car1.isEmpty());

        Optional<Car> car2 = Optional.of(new Car(new Engine()));

        Optional<Car> car3 = car2.filter(c -> c.getEngine() != null);
        System.out.println(car3.isEmpty());
    }

    private static void flatMapExample(Car car) {
        Optional<Car> optionalCar = Optional.ofNullable(car);

        Optional<Optional<Roof>> optionalRoof = optionalCar.map(Car::getRoof);
        Optional<Roof> roof = optionalCar.flatMap(Car::getRoof);
    }

    private static Optional<Car> getOptionalCar(int type) {
        if (type == 1) {
            return Optional.of(new Car());
        }

        if (type == 2) {
            return Optional.of(new Car(new Engine()));
        }

        return Optional.empty();
    }

    private static String whatIsYourCarOil(Car car) {
        return Optional.ofNullable(car)
                .map(Car::getEngine)
                .map(Engine::getOil)
                .map(Oil::getBrand)
                .orElse("NoName");
    }

    private static void driveCarNewStyle(Car car) {
        Optional<Car> optionalCar = Optional.of(car);
//        Optional<Car> optionalCar = Optional.ofNullable(car);

//        optionalCar.map(car -> car.getEngine())
        Optional<Engine> optionalEngine = optionalCar.map(Car::getEngine);

//        Engine engine = optionalEngine.orElseThrow(RuntimeException::new);
        Engine engine = optionalEngine.orElseThrow(() -> new RuntimeException("No engine"));

        Optional<Oil> optionalOil = optionalEngine.map(Engine::getOil);

        optionalOil.ifPresentOrElse(
                oil -> {
                    System.out.println("Drive far...");

                    Optional<String> optionalBrand = Optional.ofNullable(oil.getBrand());

//                    String brand = optionalBrand.orElse("NoName");
//                    String brand = optionalBrand.orElseGet(() -> "NoName");

//                    String brand = optionalBrand.orElse(getDefaultBrand());
//                    String brand = optionalBrand.orElseGet(() -> getDefaultBrand());
                    String brand = optionalBrand.orElseGet(CarUse::getDefaultBrand);
//                    Optional<String> brand = optionalBrand.or(() -> Optional.of(CarUse.getDefaultBrand()));

                    System.out.println("Brand: " + brand);
                },
                () -> System.out.println("Drive not too far...")
        );
    }

    private static String getDefaultBrand() {
        return "NoName";
    }

    private static void driveCarOldStyle(Car car) {
        if (car == null) {
            throw new RuntimeException("No car");
        }

        Engine engine = car.getEngine();

        if (engine == null) {
            throw new RuntimeException("No engine");
        }

        Oil oil = engine.getOil();

        if (oil == null) {
            System.out.println("Drive not too far...");
            return;
        }

        System.out.println("Drive far...");

        String brand = oil.getBrand();

        if (brand == null) {
            System.out.println("NoName oil, please change");
            return;
        }

        System.out.println("Brand: " + brand);
    }
}
