package lesson200406.car;

public class Oil {
    private String brand;

    public Oil(String brand) {
        this.brand = brand;
    }

    public Oil() {
    }

    public String getBrand() {
        return brand;
    }
}
