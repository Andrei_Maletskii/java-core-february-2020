package lesson200406;

public class Example {
    public static void main(String[] args) {
        Example.m1(a -> {
            System.out.println("hello");
        });
    }

    public static void m1(final D b){

    }

}
@FunctionalInterface
interface C {
    void m(final int a);
}
@FunctionalInterface
interface D extends C {

}