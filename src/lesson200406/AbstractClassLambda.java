package lesson200406;

public class AbstractClassLambda {

    public static void main(String[] args) {
        methodA(new A() {
            @Override
            public int method(int i) {
                return i * 2;
            }
        });

//        methodA((i) -> i * 2);

        methodB(i -> i * 2);
    }

    public static void methodA(A a) {
        System.out.println(a.method(1));
    }

    public static void methodB(B b) {
        System.out.println(b.method(1));
    }
}

abstract class A implements B {
    public abstract int method(int i);
}

//@FunctionalInterface
interface B {
    int method(int i); // int -> int
//    int method2(int i); // int -> int
}
