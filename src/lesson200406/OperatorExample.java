package lesson200406;

import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.UnaryOperator;

public class OperatorExample {

    public static void main(String[] args) {
        UnaryOperator<String> unaryOperator = str -> str.toUpperCase();
        UnaryOperator<String> unaryOperator1 = String::toUpperCase;
        Function<String, String> functionAnalogue;

        BinaryOperator<String> binaryOperator = (str1, str2) -> str1.concat(str2);
        BinaryOperator<String> binaryOperator1 = String::concat;
        BiFunction<String, String, String> biFunctionAnalogue;
    }
}
