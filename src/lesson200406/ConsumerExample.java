package lesson200406;

import java.util.function.Consumer;

public class ConsumerExample {

    public static void main(String[] args) {

        Consumer<String> consumer = new Consumer<String>() {
            @Override
            public void accept(String s) {
                System.out.println(s);
            }
        };

        Consumer<String> consumer1 = (String s) -> {
            System.out.println(s);
        };

        Consumer<String> consumer2 = (s) -> {
            System.out.println(s);
        };

        Consumer<String> consumer3 = s -> {
            System.out.println(s);
        };

        Consumer<String> consumer4 = s -> System.out.println(s);

        Consumer<String> consumer5 = System.out::println;
    }
}
