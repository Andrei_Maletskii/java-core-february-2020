package lesson200310;

public class State {
    int i;

    public State(int i) {
        this.i = i;
    }

    public int addAndGet() {
        return ++i;
    }

    public static void main(String[] args) {
        State state = new State(10);

        // debug here ALT+f8
        System.out.println(state.addAndGet());
    }
}
