package lesson200310;

public class CheckedUnchecked {

    public static void main(String[] args) {
        throwUnchecked();

        try {
            throwChecked();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            throw new Error();
        } catch (Error e) {

        }
    }

    public static void throwChecked() throws Exception {
        throw new Exception();
    }

    public static void throwUnchecked() {
        throw new RuntimeException();
    }
}
