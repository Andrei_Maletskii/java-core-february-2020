package lesson200310.serialization;

import java.io.*;

public class Triangle implements Serializable {

    private static final long serialVersionUID = -6849794460654667710L;

//    public static int count;
    private Point2D pointA;
    private Point2D pointB;
    private Point2D pointC;
    private transient int lineWidth = 2;

    public Triangle(Point2D pointA, Point2D pointB, Point2D pointC) {
        System.out.println("Creating triangle");
        this.pointA = pointA;
        this.pointB = pointB;
        this.pointC = pointC;
        lineWidth = 1;
//        count++;
    }

    public Triangle(Point2D pointA, Point2D pointB, Point2D pointC, int lineWidth) {
        this.pointA = pointA;
        this.pointB = pointB;
        this.pointC = pointC;
        this.lineWidth = lineWidth;
    }

    public Point2D getPointA() {
        return pointA;
    }

    public Point2D getPointB() {
        return pointB;
    }

    public Point2D getPointC() {
        return pointC;
    }

    public int getLineWidth() {
        return lineWidth;
    }

    @Override
    public String toString() {
        return "Triangle{" +
                "pointA=" + pointA +
                ", pointB=" + pointB +
                ", pointC=" + pointC +
                '}';
    }
}
