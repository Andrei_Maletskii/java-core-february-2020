package lesson200310.serialization;

import java.io.*;

public class Serialization {

    public static void main(String[] args) throws IOException {
//        Point2D pointa = new Point2D(0, 0);
        Point2D pointb = new Point2D(100, 0);
        Point2D pointc = new Point2D(0, 100);

        Triangle triangle = new Triangle(
                pointb,
                pointb,
                pointc
        );

        ObjectOutputStream oos =
                new ObjectOutputStream(
                        new FileOutputStream("triangle")
                );

        oos.writeObject(triangle);

        oos.close();
    }
}
