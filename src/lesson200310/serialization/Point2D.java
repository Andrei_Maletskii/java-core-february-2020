package lesson200310.serialization;

import java.io.Serializable;

public class Point2D implements Serializable {

    private int x;
    private int y;
//    Triangle myTriangle;

    public Point2D(int x, int y) {
        System.out.println("Creating point2D");
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public String toString() {
        return "Point2D{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
