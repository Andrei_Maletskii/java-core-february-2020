package lesson200310.serialization;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

public class Deserialization {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("triangle"));

        Object object1 = ois.readObject();
        try {
            Object object = ois.readObject();

            if (object instanceof Triangle) {
                Triangle newTriangle = (Triangle) object;
                System.out.println(newTriangle);

                System.out.println(newTriangle.getPointA() == newTriangle.getPointB());

                System.out.println(newTriangle.getLineWidth());
//                System.out.println(Triangle.count);
//                System.out.println(triangle);
//                System.out.println(triangle == newTriangle);
            }
        } catch (ClassNotFoundException | IOException e) {
//            throw new RuntimeException();
            e.printStackTrace();
            throw e;
        }
        ois.close();
    }
}
