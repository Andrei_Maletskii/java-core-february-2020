package lesson200310.serialization;

import java.io.*;

class A extends NotSerializable implements Serializable {

}

public class NotSerializable
//    implements Serializable
{

    public int count = 30;

    public NotSerializable() {
        System.out.println("Creating not serializable");
    }

    public static void main(String[] args) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);

        A a = new A();
        a.count = 40;
        oos.writeObject(a);
        oos.close();

        ObjectInputStream ois = new ObjectInputStream(
                new ByteArrayInputStream(baos.toByteArray())
        );

        try {
            Object object = ois.readObject();
            if (object instanceof A) {
                A a1 = (A) object;
                System.out.println(a1.count);

            }
        } catch (ClassNotFoundException e) {


        }
    }
}
