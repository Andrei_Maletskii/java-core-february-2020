package lesson200310.serialization;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;

public class SerializationMarking {

    public static void main(String[] args) throws IOException {
        Point2D point2D = new Point2D(15, 20);

        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("1point"));
        oos.writeObject(point2D);
        oos.close();

        ObjectOutputStream oos2 = new ObjectOutputStream(new FileOutputStream("2point"));
        oos2.writeObject(point2D);
        oos2.reset();
        oos2.writeObject(point2D);
        oos2.close();
    }
}
