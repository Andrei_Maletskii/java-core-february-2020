package lesson200310;

import java.io.InputStream;
import java.io.PrintStream;

public class SystemIO {

    public static void main(String[] args) {
        PrintStream out = System.out;
        out.println("hello");

        PrintStream err = System.err;
        err.println("error");

        InputStream in = System.in;
    }
}
