package lesson200310;

public class TryFinally {

    public static void main(String[] args) {
        int method = method();
        System.out.println(method);
    }

    public static int method() {
        try {
//            throw new Exception();
            return 1;
        } finally {
            System.out.println("finally");
        }
    }
}
