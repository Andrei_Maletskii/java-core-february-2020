package lesson200310;

public class ExceptionExamples {

    public static void main(String[] args) {
        try {

        } catch (Exception e) {

        }

        try {
            method();
        } catch (Exception e) {
//            e.printStackTrace();
            System.out.println(e.getMessage());
        } finally {

        }

    }

    public static void method() {
        try {
            throw new RuntimeException("ex");
        } finally {
            System.out.println("finally");
        }
    }
}
