package lesson200326;

public class UsingThreads {

    public static void main(String[] args) {
//        MyThread thread = new MyThread();
//        thread.run(); WRONG


//        thread.start();

        Thread threadWithRunnable1 = new Thread(new MyRunnable());
        Thread threadWithRunnable2 = new Thread(new MyRunnable());
//        threadWithRunnable.run(); WRONG
        threadWithRunnable1.start();
        threadWithRunnable2.start();

        System.out.println("main finished");
//        new MyRunnable().run();



//        for (int i = 0; i < 5; i++) {
//            System.out.println("hola " + i);
//            try {
//                Thread.sleep(1000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
    }
}

class MyThread extends Thread {
    @Override
    public void run() {
        for (int i = 0; i < 5; i++) {
            System.out.println("hello " + i);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

class MyRunnable implements Runnable {
    @Override
    public void run() {
        Thread thread = Thread.currentThread();

        String name = thread.getName();

        for (int i = 0; i < 5; i++) {
            System.out.println("hi " + name + " " + i);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
