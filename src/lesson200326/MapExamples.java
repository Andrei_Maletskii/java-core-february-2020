package lesson200326;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class MapExamples {

    public static void main(String[] args) {
//        Map<Key, String> map = new HashMap<>();
        Map<Key, String> map = new TreeMap<>();

        map.put(new Key("key1"), "value1");
        map.put(new Key("key2"), "value2");
        map.put(new Key("key3"), "value3");

        System.out.println(map.get(new Key("key1")));
        System.out.println(map.get(new Key("key2")));
        System.out.println(map.get(new Key("key3")));
    }
}

class Key implements Comparable<Key> {
    private String key;

    public Key(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//
//        Key key1 = (Key) o;
//
//        return key != null ? key.equals(key1.key) : key1.key == null;
//    }
//
//    @Override
//    public int hashCode() {
//        return key != null ? key.hashCode() : 0;
//    }

    @Override
    public int compareTo(Key o) {
        return this.getKey().compareTo(o.getKey());
    }
}
