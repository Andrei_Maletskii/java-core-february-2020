package lesson200407.stream.persons;

import java.util.*;
import java.util.stream.Collectors;

public class MapAndCollectExamples {
    public static String personToCarBrand(Person person) {
        return person.getCar().getBrand();
    }

    public static void main(String[] args) {
        List<Person> personList = getDefaultPersonList();

        Set<Car> cars = personList.stream() // get stream from source
                .map(Person::getCar) // apply transform
//                .cars(Collectors.toList()); // terminal action
                .collect(Collectors.toSet());

        System.out.println(cars);

        List<String> uniqueBrands = personList.stream()
                .map(Person::getCar)
                .map(Car::getBrand)
//                .collect(Collectors.toSet())
                .distinct()
                .collect(Collectors.toList());

        System.out.println(uniqueBrands);

        String uniqueBrandsInString = personList.stream()
                .map(person -> person.getCar().getBrand())
                .distinct()
                .collect(Collectors.joining(","));

        System.out.println(uniqueBrandsInString);

        String brandsInString1 = personList.stream()
                .map(MapAndCollectExamples::personToCarBrand)
//                .distinct()
                .collect(Collectors.joining(",", "Brands ", " look good"));

        System.out.println(brandsInString1);

//        Map<String, Car> map = personList.stream()
//                .collect(Collectors.toMap(
//                            Person::getName,
//                            Person::getCar
//                        )
//                );

        Map<String, Car> map = personList.stream()
                .collect(
                        Collectors.toMap(
                                Person::getName,
                                Person::getCar,
                                (car1, car2) -> car1
                        )
                );

        System.out.println(map);

        TreeMap<String, Car> treeMap = personList.stream()
                .collect(Collectors.toMap(
                        Person::getName,
                        Person::getCar,
                        (car1, car2) -> car1,
                        TreeMap::new
                ));

        System.out.println(treeMap);

        StringBuilder simpleSb = personList.stream().collect(
                () -> new StringBuilder(),
                (sb, person) -> sb.append(person),
                (sb1, sb2) -> sb1.append(sb2)
        );

        StringBuilder methodRefSb = personList.stream().collect(
                StringBuilder::new,
                StringBuilder::append,
                StringBuilder::append
        );

        String animalNames = personList.stream()
                .map(Person::getAnimals)
                .flatMap(Collection::stream)
                .map(Animal::getName)
                .collect(Collectors.joining(",", "All animals are: ", ""));

        System.out.println(animalNames);

        String animalNames1 = personList.stream()
                .map(Person::getAnimals)
                .map(Collection::stream)
                .flatMap(s -> s)
                .map(Animal::getName)
                .collect(Collectors.joining(",", "All animals are: ", ""));

        Map<Car, List<Person>> map1 = personList.stream()
                .collect(Collectors.groupingBy(Person::getCar));

        System.out.println(map1);

        Map<Car, Set<Person>> map2 = personList.stream()
                .collect(Collectors.groupingBy(
                        Person::getCar,
                        Collectors.toSet())
                );

        TreeMap<Car, Set<Person>> map3 = personList.stream()
                .collect(
                        Collectors.groupingBy(
                                Person::getCar,
                                TreeMap::new,
                                Collectors.toSet()
                        )
                );

        System.out.println(map3);
    }

    public static List<Person> getDefaultPersonList() {
        return Arrays.asList(
                new Person(
                        "Amy",
                        new Car("Toyota"),
                        Arrays.asList(new Animal("Bobik", 1))
                ),
                new Person(
                        "Bob",
                        new Car("Toyota"),
                        Arrays.asList(new Animal("Sharik", 10), new Animal("Belka", 5))
                ),
                new Person(
                        "Amy",
                        new Car("Volga"),
                        Arrays.asList(new Animal("Strelka", 15))
                )
        );
    }
}






