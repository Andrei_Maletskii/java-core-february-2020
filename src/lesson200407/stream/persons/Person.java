package lesson200407.stream.persons;

import java.util.List;

public class Person {
    private String name;
    private Car car;
    private List<Animal> animals;

    public Person(String name, Car car, List<Animal> animals) {
        this.name = name;
        this.car = car;
        this.animals = animals;
    }

    public String getName() {
        return name;
    }

    public Car getCar() {
        return car;
    }

    public List<Animal> getAnimals() {
        return animals;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                '}';
    }
}
