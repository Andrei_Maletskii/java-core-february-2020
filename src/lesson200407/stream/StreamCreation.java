package lesson200407.stream;

import lesson200406.person.Person;

import java.util.*;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamCreation {
    public static void main(String[] args) {
        Collection<String> collection = new ArrayList<>();

        Stream<String> stream = collection.stream();

        Stream<Integer> empty = Stream.empty();

        Stream<Integer> stream1 = Stream.of(0);

        Integer[] array = {1, 2, 3, 4, 5};

        Stream<Integer> array1 = Stream.of(array);

        Stream<Integer> stream2 = Stream.of(1, 2, 3, 4, 5);

        Stream<Integer> emptyStream = Stream.ofNullable(null);

        Stream<Integer> stream3 = Stream.of(1, 2, 3);
        Stream<Integer> stream4 = Stream.of(4, 5, 6);

        Stream<Integer> concatResult = Stream.concat(stream3, stream4);

        Stream.Builder<Integer> builder = Stream.builder();
        builder.add(1).add(2).add(3);
        builder.accept(4);
        builder.accept(5);
        Stream<Integer> builtStream = builder.build();


        Stream<Integer> generatedStream0 = Stream.generate(() -> 17).limit(20);

        Random random = new Random();
        Stream<Integer> generatedStream = Stream.generate(random::nextInt);

        Stream<Integer> limited = generatedStream.limit(20);

        limited.forEach(System.out::println);

        System.out.println("-----");

        Stream.iterate(0, i -> i + 2)
                .skip(1)
                .limit(20)
                .forEach(System.out::println);

        System.out.println("-----");

        Stream.iterate(
                "0",
                s -> s.length() < 10,
                s -> s + "0"
        ).forEach(System.out::println);

        Optional<String> optionalString = Optional.of("hello");
        Stream<String> stream5 = optionalString.stream();

        int[] ints = {1,2,3,4,5};

        IntStream stream6 = Arrays.stream(ints);

    }

    private static void process(Person person) {
        Stream<Person> empty = Stream.empty();
    }
}
