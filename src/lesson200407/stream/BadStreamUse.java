package lesson200407.stream;

import java.util.stream.Stream;

public class BadStreamUse {

    public static void main(String[] args) {
        Stream<Integer> intStream = Stream.of(1, 2, 3, 4, 5);

        Stream<Integer> skip = intStream.skip(1);

        Stream<Integer> limit = skip.limit(3);

        limit.forEach(System.out::println);

        System.out.println("---");

        Stream.of(1,2,3,4,5)
                .skip(1)
                .limit(3)
                .forEach(System.out::println);

        Stream<Integer> wrongUseStream = Stream.of(1, 2, 3, 4, 5);

        wrongUseStream.skip(1);
        // WRONG USE
        wrongUseStream.limit(3);

        wrongUseStream.forEach(System.out::println);
    }
}
