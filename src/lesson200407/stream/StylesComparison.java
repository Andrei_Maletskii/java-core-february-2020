package lesson200407.stream;

import lesson200406.person.Computer;
import lesson200406.person.Person;
import lesson200406.person.Processor;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StylesComparison {

    public static void main(String[] args) {
        List<Person> list = getPersonList();

        // Stream API
        // ETL - Extract Transform Load
        // 1) get stream from source
        // 2) transform
        // 3) terminal action

        list.stream()
                .map(Person::getName)
                .forEach(System.out::println);

        List<String> processorNames = list.stream()
                .map(person -> person.getComputer().getProcessor().getName())
                .collect(Collectors.toList());

        String processorNames2 = list.stream()
                .map(Person::getComputer)
                .map(Computer::getProcessor)
                .map(Processor::getName)
//                .collect(Collectors.toList());
                .collect(Collectors.joining(","));

        System.out.println(processorNames);
        System.out.println(processorNames2);


    }

    private static void printPersonsInfo(List<Person> personList) {
        List<String> personNames = new ArrayList<>();

        for (Person person : personList) {
            personNames.add(person.getName());

            System.out.println(person.getAge());

            String name = person.getComputer().getProcessor().getName();

            System.out.println(name);
        }
    }

    public static List<Person> getPersonList() {
        List<Person> personList = new ArrayList<>();

        personList.add(new Person(
                        new Computer(new Processor("Intel i9")), "Amy", 20)
        );
        personList.add(
                new Person(
                        new Computer(new Processor("Intel i7")), "Fedor", 22)
        );
        personList.add(
                new Person(
                        new Computer(new Processor("Intel i5")), "Foma", 26)
        );
        personList.add(
                new Person(
                        new Computer(new Processor("Intel i3")), "Ivan", 32)
        );
        personList.add(
                new Person(
                        new Computer(new Processor("AMD Ryzen")), "Amy", 26
                )
        );

        return personList;
    }
}
