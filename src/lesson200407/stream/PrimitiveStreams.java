package lesson200407.stream;

import lesson200407.stream.persons.Animal;
import lesson200407.stream.persons.MapAndCollectExamples;
import lesson200407.stream.persons.Person;

import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class PrimitiveStreams {

    public static void main(String[] args) {
        IntStream zeroToNine = IntStream.range(0, 10);
        IntStream zeroToTen = IntStream.rangeClosed(0, 10);

        int sum = zeroToNine.sum();

        Stream<Integer> boxed = IntStream.range(0, 10).boxed();
        IntStream intStream = boxed.mapToInt(i -> i);

        List<Person> personList = MapAndCollectExamples.getDefaultPersonList();

        IntStream ageStream = personList.stream()
                .map(Person::getAnimals)
                .flatMapToInt(animals -> animals.stream().mapToInt(Animal::getAge));

        IntSummaryStatistics intSummaryStatistics = ageStream.summaryStatistics();

//        intStream.sum();
//        intStream.average();
//        intStream.max();
//        intStream.min();
//        intStream.count();

        System.out.println(intSummaryStatistics);

    }
}
