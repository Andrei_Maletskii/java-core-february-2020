package lesson200323;

import java.util.*;

public class NavigableSetExample {

    public static void main(String[] args) {
        NavigableSet<Integer> ns = new TreeSet<>();

        ns.add(1);
        ns.add(3);
        ns.add(5);
        ns.add(7);
        ns.add(9);

        System.out.println(ns);

        Integer element = ns.first();

        while (element != null) {
            System.out.println("elem = " + element);

            element = ns.higher(element);
        }

        Set<String> objects = new TreeSet<>();
        Set<String> object1 = new HashSet<>();
        Set<String> object2 = new LinkedHashSet<>();

        new TreeSet<>(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return 0;
            }
        });
    }
}
