package lesson200323;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CollectionExamples {

    public static void main(String[] args) {
        Object obj1 = new Object();
        Object obj2 = new Object();
        Object obj3 = new Object();

        List<Object> objects = Arrays.asList(obj1, obj2, obj3);

        Object[] array = {new Object(), new Object()};

        List<Object> objects1 = Arrays.asList(array);

        List<Object> collect = Stream.of(array).collect(Collectors.toList());

        Set<Integer> singleton = Collections.singleton(1);
        List<Integer> integers = Collections.singletonList(1);
        Map<Integer, String> hello = Collections.singletonMap(1, "hello");


        List<Integer> collect1 = Stream.of(1, 2, 3, 4, 5).peek(System.out::println).collect(Collectors.toList());

        System.out.println(collect1);
    }
}
