package lesson200305;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;

public class ReadAndWrite {

    public static void main(String[] args) throws IOException {
        byte[] data = new byte[256];
        int j = 0;
        for (int i = -128; i <= 127; i++) {
            data[j] = (byte) i;
            j++;
        }

        System.out.println(Arrays.toString(data));

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        baos.write(data);

        byte[] dataFromOutputStream = baos.toByteArray();

        ByteArrayInputStream bais = new ByteArrayInputStream(dataFromOutputStream);

        int k = 0;
        int e;
//        while ((e = bais.read()) != -1) {
        while (bais.available() > 0) {
//            System.out.println(data[k] + " -> " + e);
            System.out.println(data[k] + " -> " + bais.read());
//            if (k == 5) {
//                bais.mark(100);
//            }
//            if (k == 10) {
//                bais.reset();
//            }
            k++;
        }

//        bais.close();
//        baos.close();

    }
}
