package lesson200305;

import java.math.BigInteger;

public class Fibonacci {

    public static int fibSlow(int n) {
        if (n < 0) {
            throw new IllegalArgumentException();
        }

        if (n <= 1) {
            return n;
        }

        return fibSlow(n - 2) + fibSlow(n - 1);
    }

    // need to expand array if necessary
    private static BigInteger[] cache = new BigInteger[1000];

    static {
        cache[0] = BigInteger.ZERO;
        cache[1] = BigInteger.ONE;
    }

    public static BigInteger fibFast(int n) {
        if (n < 0) {
            throw new IllegalArgumentException();
        }

//        if (n <= 1) { // can be replaced by setting first two values in cache
//            return BigInteger.valueOf(n);
//        }

//        if (n > cache.length) {
//            BigInteger[] newCache = new BigInteger[cache.length * 2];
//            System.arraycopy();
//        }

        if (cache[n] != null) {
            return cache[n];
        }

        BigInteger value = fibFast(n - 1).add(fibFast(n - 2));
        cache[n] = value;
        return value;
    }

    private static void estimateFib(int n) {
        long start = System.nanoTime();
        BigInteger result = fibFast(n);
        long finish = System.nanoTime();

        System.out.println("Result " + n + " = " + result + " time: " + (finish - start));
    }

    public static void main(String[] args) {
        for (int i = 0; i < 1000; i++) {
            estimateFib(i);
        }

        try {
            fibFast(-1);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }
}
