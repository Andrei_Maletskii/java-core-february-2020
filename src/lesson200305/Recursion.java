package lesson200305;

public class Recursion {

    public static void main(String[] args) {
        method(10);
    }

    public static void method(int i) {
        if (i < 0) {
            return;
        }

        System.out.println(i);

        method(i - 1);
    }
}
