package lesson200319;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class ExceptionExamples {

    public static void main(String[] args) throws FileException {

        try {
            throwUncheckedException();
        } catch (RuntimeException e) {
            System.out.println(e.getMessage());
        }

        try {
            throwCheckedException();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            throwUncheckedException();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            throwMyException1();
            throwMyException2();
        } catch (MyException1 | MyException2 | ArithmeticException e) {
            e.printStackTrace();
        }

        try {
            try {
                throwMyException2();
            } catch (MyException1 e) {
                e.printStackTrace();
            }
        } catch (MyException2 e) {
            e.printStackTrace();
        }

        try {
            if (true) {
                throw new FileException();
            }
            throw new NetworkException();
        } catch (FileException e) {
            throw new RuntimeException();
        } catch (NetworkException e) {

        } catch (IOException e) {

        }

        try (FileOutputStream fis = new FileOutputStream("hello");
             ObjectOutputStream oos = new ObjectOutputStream(fis)) {
            oos.writeObject(1);
            fis.write(1);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        throwRuntimeException();
        throwFileException();
    }

    private static void throwRuntimeException() throws RuntimeException {
        throw new RuntimeException();
    }

    private static void throwFileException() throws FileException {
        throw new FileException();
    }

    private static class FileException extends IOException {

    }

    private static class NetworkException extends IOException {

    }

    private static void throwMyException1() {
        MyException1 myException1 = new MyException1();
        throw myException1;
    }

    private static void throwMyException2() {
        throw new MyException2("message2", 0);
    }

    private static void throwUncheckedException() {
        throw new RuntimeException("message");
    }

    private static void throwCheckedException() throws Exception {
        throw new Exception();
    }
}

class MyException1 extends RuntimeException {

}

class MyException2 extends RuntimeException {

    int id;

    public MyException2(String message) {
        super(message);
    }

    public MyException2(String message, int id) {
        super(message);
        this.id = id;
    }
}
