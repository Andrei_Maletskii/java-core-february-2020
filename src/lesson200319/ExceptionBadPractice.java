package lesson200319;

public class ExceptionBadPractice {

    public static void main(String[] args) {

        Number i = null;

        try {
            if (args[0].equals("hello")) {
                i = 1;
            } else {
                i = 1.5f;
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            i = 1.7;
        }

        System.out.println(i);
        System.out.println(check(args));
    }

    public static Number check(String[] args) {
        if (args.length == 0) {
            return 1.7;
        }

        if (args[0].equals("hello")) {
            return 1;
        }

        return 1.5f;
    }
}
