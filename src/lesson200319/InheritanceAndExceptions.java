package lesson200319;

import java.io.EOFException;
import java.io.IOException;

public class InheritanceAndExceptions {

}

class BaseCl {
    public BaseCl() throws IOException, ArithmeticException {

    }

    public void methodA() throws IOException {

    }

    public static void methodB() throws IOException {

    }
}

class DerivativeCl extends BaseCl {

    public DerivativeCl() throws ClassNotFoundException, IOException, ArithmeticException {
        super();
    }

    public void methodA() throws EOFException {

    }

    public static void methodB() throws EOFException {

    }
}

class DerivativeCl2 extends BaseCl {
    // ошибок компиляции нет
    public DerivativeCl2() throws Exception {
        super();
    }

    // compile error
//    public void methodA() throws Exception {
//        throw new Exception();
//    }

//    public static void methodB() throws Exception {
//    }
}

