package lesson200319;

import java.rmi.RemoteException;

public class ThrowsExample {

    public static void remoteMethod() throws RemoteException {
        try {
            throw new RemoteException("this is RemoteException");
        } catch (Exception e) {
            throw e;
        }
    }
}
