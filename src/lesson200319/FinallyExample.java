package lesson200319;

public class FinallyExample {

    public static void main(String[] args) {
//        procA();
        int x = procB();
        System.out.println(x);
    }

    static void procA() {
        try {
            System.err.println("Внутри procA");
            throw new RuntimeException("demo");
        } finally {
            System.err.println("finally для procA ");
        }
    }

    static int procB() {
        try {
            System.err.println("Внутри procB");
            return 1;
        } finally {
            System.err.println("finally для procB ");
        }
    }
}
