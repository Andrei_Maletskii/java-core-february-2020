package lesson200319;

public class FinallyFeatures {

    private static int age = 20;


    public static int getAgeWoman() {
        try {
            return age - 3;
        } catch (RuntimeException e) {
            return age - 6;
        } finally {
            return age;
        }
    }


    public static void main(String[] args) {
//        System.out.println(getAgeWoman());
//        breakFinally();
        finallyRules();
    }

    public static void breakFinally() {
        try {
            System.exit(-100);
        } finally {
            System.err.println("Finally executed");
        }
    }

    public static void finallyRules() {
        try {
            throw new RuntimeException("hello");
        } catch (ArithmeticException e) {
            throw new RuntimeException("hi");
        } finally {
            throw new ArithmeticException("/ by zero");
        }
    }


}
