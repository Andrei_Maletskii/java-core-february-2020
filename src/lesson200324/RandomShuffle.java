package lesson200324;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class RandomShuffle {

    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);

        Random rnd = new Random(10);
        Collections.shuffle(list, rnd);
        System.out.println(list);

        Collections.shuffle(list, rnd);
        System.out.println(list);
    }
}
