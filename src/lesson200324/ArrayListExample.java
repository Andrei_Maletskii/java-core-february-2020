package lesson200324;

import java.util.ArrayList;

public class ArrayListExample {

    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>(1);

        list.ensureCapacity(4);

        list.add("one");
        list.add("two");
        list.add("three");
        list.add("four");

        System.out.println(list);

    }
}
