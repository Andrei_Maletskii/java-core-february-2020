package lesson200324;

import java.util.*;
import java.util.function.BiConsumer;

public class EnhancedBenchmark {


    private static final int N = 100_000;

    public static void main(String[] args) {
        long arrayListResult1 = estimate(new ArrayList<>(), EnhancedBenchmark::fillAtTheEnd);
        System.out.println("ArrayList add to the end: " + arrayListResult1);

        long arrayListResultCapacity1 = estimate(new ArrayList<>(N), EnhancedBenchmark::fillAtTheEnd);
        System.out.println("ArrayList add to the end with set up capacity: " + arrayListResultCapacity1);

        long linkedListResult1 = estimate(new LinkedList<>(), EnhancedBenchmark::fillAtTheEnd);
        System.out.println("LinkedList add to the end: " + linkedListResult1);

        long arrayListResult2 = estimate(new ArrayList<>(), EnhancedBenchmark::fillAtTheHead);
        System.out.println("ArrayList add at the start: " + arrayListResult2);

        long arrayListResultCapacity2 = estimate(new ArrayList<>(N), EnhancedBenchmark::fillAtTheHead);
        System.out.println("ArrayList add at the start with capacity: " + arrayListResultCapacity2);

        long linkedListResult2 = estimate(new LinkedList<>(), EnhancedBenchmark::fillAtTheHead);
        System.out.println("LinkedList add at the start: " + linkedListResult2);
    }

    private static void fillAtTheEnd(List<Integer> list, Collection<Integer> data) {
        for (Integer dataElement : data) {
            list.add(dataElement);
        }
    }

    private static void fillAtTheHead(List<Integer> list, Collection<Integer> data) {
        for (Integer dataElement : data) {
            list.add(0, dataElement);
        }
    }

    private static long estimate(
            List<Integer> list,
            BiConsumer<List<Integer>, Collection<Integer>> methodToEstimate
    ) {
        ArrayList<Integer> data = new ArrayList<>(N);
        Random random = new Random();
        for (int i = 0; i < N; i++) {
            data.add(random.nextInt());
        }

        long start = System.currentTimeMillis();
        methodToEstimate.accept(list, data);
        long stop = System.currentTimeMillis();
        return stop - start;
    }
}
