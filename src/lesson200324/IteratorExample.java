package lesson200324;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.function.Predicate;

public class IteratorExample {

    public static void main(String[] args) {

        ArrayList<Integer> ints = new ArrayList<>() {
            {
                add(1);
                add(2);
                add(3);
                add(4);
                add(5);
                add(6);
            }
        };

        for (Integer anInt : ints) {
            System.out.println(anInt);
        }

        ListIterator<Integer> iterator = ints.listIterator();

        while (iterator.hasNext()) {
            Integer integer = iterator.next();

            System.out.println(integer);

//            iterator.remove();
        }

        ListIterator<Integer> listIter = ints.listIterator(5);

        while (listIter.hasPrevious()) {
            Integer integer = listIter.previous();

            System.out.println(integer);
        }


        System.out.println("Bad example");
        for (Integer anInt : ints) {
            if (anInt % 2 == 0) {

//                ints.remove(anInt);
            }
        }

        Iterator<Integer> iterator1 = ints.iterator();

        while (iterator1.hasNext()) {
            Integer next = iterator1.next();

            if (next % 2 == 0) {
                iterator1.remove();
            }
        }

        System.out.println("current collection: " + ints);

        // second approach
//        ints.removeIf(new Predicate<Integer>() {
//            @Override
//            public boolean test(Integer integer) {
//                return integer % 2 == 0;
//            }
//        });


    }
}
