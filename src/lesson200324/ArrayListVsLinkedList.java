package lesson200324;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ArrayListVsLinkedList {

    public static void main(String[] args) {
        method();
    }

    public static void method() {
        List<String> list = new ArrayList<>();
        list.add("A");
        list = new LinkedList<String>();
        System.out.println(list);
    }
}
