package lesson200324;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class InterfaceVsImplementation {

    public static void main(String[] args) {
        fill(new ArrayList<>(), 1);
        fill(new LinkedList<>(), 1);
    }

    public static <E> void fill(Collection<E> collection, E element) {
        for (int i = 0; i < 10; i++) {
            collection.add(element);
        }
    }
}
