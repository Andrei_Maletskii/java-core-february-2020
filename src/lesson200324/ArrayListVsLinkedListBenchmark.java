package lesson200324;

import java.util.*;

public class ArrayListVsLinkedListBenchmark {


    private static final int N = 100_000;

    public static void main(String[] args) {
//        long arrayListResult1 = estimateAddToTheEnd(new ArrayList<>());
//        System.out.println("ArrayList add to the end: " + arrayListResult1);
//
//        long arrayListResultCapacity = estimateAddToTheEnd(new ArrayList<>(N));
//        System.out.println("ArrayList add to the end with set up capacity: " + arrayListResultCapacity);
//
//        long linkedListResult1 = estimateAddToTheEnd(new LinkedList<>());
//        System.out.println("LinkedList add to the end: " + linkedListResult1);

        long arrayListResult2 = estimateAddAtTheStart(new ArrayList<>());
        System.out.println("ArrayList add at the start: " + arrayListResult2);

        long arrayListResultCapacity = estimateAddAtTheStart(new ArrayList<>(N));
        System.out.println("ArrayList add at the start with capacity: " + arrayListResultCapacity);

        long linkedListResult2 = estimateAddAtTheStart(new LinkedList<>());
        System.out.println("LinkedList add at the start: " + linkedListResult2);
    }

    private static void fillAtTheEnd(List<Integer> list, Collection<Integer> data) {
        for (Integer dataElement : data) {
            list.add(dataElement);
        }
    }

    private static void fillAtTheHead(List<Integer> list, Collection<Integer> data) {
        for (Integer dataElement : data) {
            list.add(0, dataElement);
        }
    }



    private static long estimateAddToTheEnd(List<Integer> list) {
        ArrayList<Integer> data = new ArrayList<>(N);
        Random random = new Random();
        for (int i = 0; i < N; i++) {
            data.add(random.nextInt());
        }

        long start = System.currentTimeMillis();
        fillAtTheEnd(list, data);
        long stop = System.currentTimeMillis();
        return stop - start;
    }

    private static long estimateAddAtTheStart(List<Integer> list) {
        ArrayList<Integer> data = new ArrayList<>(N);
        Random random = new Random();
        for (int i = 0; i < N; i++) {
            data.add(random.nextInt());
        }

        long start = System.currentTimeMillis();
        fillAtTheHead(list, data);
        long stop = System.currentTimeMillis();
        return stop - start;
    }
}
