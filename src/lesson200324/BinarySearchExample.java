package lesson200324;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BinarySearchExample {

    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();

        list.add(5);
        list.add(3);
        list.add(7);
        list.add(1);
        list.add(2);
        list.add(8);

        System.out.println(list);

        Collections.sort(list, Collections.reverseOrder());

        System.out.println(list);

        int i = Collections.binarySearch(list, 2, Collections.reverseOrder());

        System.out.println(i);

        Collections.rotate(list, 3);

        System.out.println(list);
    }
}
