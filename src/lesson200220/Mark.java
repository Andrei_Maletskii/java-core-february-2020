package lesson200220;

import java.util.List;

public class Mark<T> {
    private final T value;

    public Mark(T value) {
        this.value = value;
    }

    private <E extends Comparable<E>> void process(E e1, E e2) {
        e1.compareTo(e2);
    }

    public <K> int process(K k, T t) {
        return 0;
    }

    private <H> int process(H h) {
        return 0;
    }

    private <E extends Comparable<E>> void process(List<? super E> eList, E e) {
//        e.compareTo();
        eList.add(e);
    }

    public static void main(String[] args) {
        Mark<Integer> mark = new Mark<>(45);

        mark.process(46.5f, 44.4f);

        mark.process('a', 'a');

        mark.process("String", 4);

    }
}
