package lesson200220;

public enum Day {
    SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY;

    public boolean isWeekend() {
        switch (this) {
            case SUNDAY:
            case SATURDAY:
                return true;
            default:
                return false;
        }
    }

    public static void main(String[] args) throws ClassNotFoundException {
        boolean canI = canGoToCinema(Day.valueOf("SUNDAY"));
        boolean canIToday = canGoToCinema(Day.THURSDAY);

        Day[] days = Day.values();

        for (Day day : days) {

        }

        Enum.valueOf(Day.class, "SUNDAY");
    }

    public static boolean canGoToCinema(Day day) {
        return day.isWeekend();
    }
}
