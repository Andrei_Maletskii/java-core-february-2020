package lesson200220;

public class Main {

    public static void main(String[] args) {
        Container1 container1 = new Container1(1);
        change(container1);
        System.out.println(container1);
    }

    private static void change(Container1 i) {
//        i.value = 30;
        i = new Container1(30);
    }
}

class Container1 {
    public final int value;

    public Container1(int value) {
        this.value = value;
    }
}
