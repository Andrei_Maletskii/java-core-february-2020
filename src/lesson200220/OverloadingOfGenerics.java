package lesson200220;

import java.util.List;

public class OverloadingOfGenerics {
//    public static void process(List<Integer> integers) {
//
//    } // ERROR due to erasure

    public static void process(List<Moveable> ys) {
        for (Moveable y : ys) {
            y.move();
        }
    }

}

class X implements Moveable {}
class Y implements Moveable {}

interface Moveable {
    default void move() {

    }
}
