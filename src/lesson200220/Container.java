package lesson200220;

import java.util.ArrayList;
import java.util.List;

public class Container<T> {

    private T value;

    public Container(T value) {
        this.value = value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public T getValue() {
        return value;
    }

    public static <V> void copy(Container<? super V> dest, Container<? extends V> src) {
        V value = src.getValue();
//        src.setValue(value); ERROR

        dest.setValue(value);
        Object value1 = dest.getValue();
    }

    // PECS - Producer Extends Consumer Super

    public static void main(String[] args) {
        Container<A> a = new Container<>(new A());
        Container<B> b = new Container<>(new B());
        Container<C> c = new Container<>(new C());

        Container.<A>copy(a, a);
        Container.<B>copy(b, b);
        Container.<C>copy(c, c);

        Container.<B>copy(a, b);
        Container.<C>copy(b, c);

//        copy(b, a);
//        copy(c, b);


        A a1 = new A();
        C c1 = (C) a1;

        List<? extends B> list = new ArrayList<C>();
        B b1 = list.get(0);
        List<? extends B> list1 = new ArrayList<B>();
        B b2 = list1.get(0);
//        List<? extends B> list2 = new ArrayList<A>();

        process(new ArrayList<C>());
        process(new ArrayList<B>());
        process(new ArrayList<D>());

        process1(new ArrayList<Object>());
        process1(new ArrayList<A>());
        process1(new ArrayList<B>());
//        process1(new ArrayList<C>());
    }

    public static <E> void process1(List<? super B> list) {
        Object object = list.get(0);

//        list.add(new Object())
//        list.add(new A());
        list.add(new B());
        list.add(new C());
    }

    public static <E> void process(List<? extends B> list) {
        B b = list.get(0);
//        list.add(new B());
//        list.add(new A());
//        list.add(new C());
    }

}

class A {}
class B extends A {}
class C extends B {}
class D extends C {}