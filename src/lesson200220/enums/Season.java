package lesson200220.enums;

public enum Season implements LeafColor {
    WINTER {
        @Override
        public int getLeafColor() {
            return 0;
        }
    },
    SUMMER {
        @Override
        public int getLeafColor() {
            return 0;
        }
    },
    AUTUMN {
        @Override
        public int getLeafColor() {
            return 0;
        }
    },
    SPRING {
        @Override
        public int getLeafColor() {
            return 0;
        }
    };


}

interface LeafColor {
    public abstract int getLeafColor();
}


