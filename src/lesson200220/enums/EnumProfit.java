package lesson200220.enums;

public class EnumProfit {

    public static void process(Command command) {
        command.action();
    }

    public static void main(String[] args) {
        process(Command.RUN);
        process(Command.STOP);
        process(Command.TURN_LEFT);

        Command run = Command.valueOf("RUN");
        System.out.println(run == Command.RUN);

        Command.Inner inner = Command.RUN.new Inner();
    }
}

enum Command {
    RUN("run"),
    STOP("stop"),
    TURN_LEFT("to the left"),
    TURN_RIGHT("to the right");

    private String message;

    private Command(String message) {
        this.message = message;
    }

    public void action() {
        System.out.println(message);
    }

    class Inner {

    }
}
