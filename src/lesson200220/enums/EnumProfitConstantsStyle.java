package lesson200220.enums;

public class EnumProfitConstantsStyle {
    
    public static final String RUN = "RUN";
    public static final String STOP = "STOP";
    public static final String TURN_LEFT = "TURN_LEFT";
    public static final String TURN_RIGHT = "TURN_RIGHT";
    
    
    public static void process(String command) {
        switch (command) {
            case RUN -> System.out.println("run");
            case STOP -> System.out.println("stop");
            case TURN_LEFT -> System.out.println("to the left");
            case TURN_RIGHT -> System.out.println("to the right");
            default -> System.out.println("default action");
        }
    }

    public static void main(String[] args) {
        process(RUN);
        process(STOP);
        process("SIT");

        Season winter = Season.valueOf("WINTER");
        Season unknown = Season.valueOf("hello");
    }
}
