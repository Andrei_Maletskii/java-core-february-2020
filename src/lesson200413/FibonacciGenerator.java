package lesson200413;

import java.math.BigInteger;

public class FibonacciGenerator {

    private BigInteger[] cache;

    {
        clearCache();
    }

    public BigInteger fibFast(int n) {
        if (n < 0) {
            throw new IllegalArgumentException();
        }

        if (cache[n] != null) {
            return cache[n];
        }

        BigInteger value = fibFast(n - 1).add(fibFast(n - 2));
        cache[n] = value;
        return value;
    }

    public void clearCache() {
        cache = new BigInteger[1000];

        cache[0] = BigInteger.ZERO;
        cache[1] = BigInteger.ONE;
    }
}
