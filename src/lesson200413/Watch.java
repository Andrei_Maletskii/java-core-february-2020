package lesson200413;

public class Watch {
    private static final short DEFAULT_HOUR = 12;
    private Watch() {
        super();
    }
    int checkHour() {
        assert DEFAULT_HOUR > 12;
        return DEFAULT_HOUR;
    }
    public static void main(String... ticks) {
        new Watch().checkHour();
    }
}
