package lesson200413.reflection;

public class Target {

    private Target(Integer integer) {
    }

    private String targetMethod(boolean flag) {
        if (flag) {
            return "The boolean flag is true!";
        }

        return "The boolean flag is false!";
    }

    private String targetMethod(Boolean flag) {
        if (flag) {
            return "The Boolean flag is true!";
        }

        return "The Boolean flag is false!";
    }

    public static Target getInstance() {
        return new Target(7);
    }
}
