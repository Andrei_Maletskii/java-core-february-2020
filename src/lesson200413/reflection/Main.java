package lesson200413.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {

    public static void main(String[] args)
            throws ClassNotFoundException,
            NoSuchMethodException,
            IllegalAccessException,
            InvocationTargetException,
            InstantiationException {
        Class<?> targetClass = Class.forName("lesson200413.reflection.Target");
//        Class<Target> targetClass = Target.class;

        System.out.println(targetClass);

//        Constructor<?>[] declaredConstructors = targetClass.getDeclaredConstructors();
//        Constructor<?>[] declaredConstructors = targetClass.getConstructors();
        Constructor<?> declaredConstructor = targetClass.getDeclaredConstructor();

        declaredConstructor.setAccessible(true);

        Object targetObject = declaredConstructor.newInstance();

        System.out.println(targetObject);

        Class<Boolean> booleanClass = Boolean.class;
        Class<Boolean> booleanClass1 = boolean.class;
        Class<Boolean> booleanType = Boolean.TYPE;

        System.out.println(booleanClass1 == booleanType);
        System.out.println(booleanClass.equals(booleanClass1));

//        Target target = (Target) targetObject;

//        Method[] declaredMethods = targetClass.getMethods();
//        Method targetMethod = targetClass.getDeclaredMethod("targetMethod", Boolean.class);
        Method targetMethod = targetClass.getDeclaredMethod("targetMethod", boolean.class);

        targetMethod.setAccessible(true);

        Object result = targetMethod.invoke(targetObject, false);

        System.out.println(result);

//        targetClass.getAnnotation(Deprecated.class)

    }
}
