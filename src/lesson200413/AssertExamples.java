package lesson200413;

public class AssertExamples {

    public static void main(String[] args) {
        int a = 3;
        int b = -5;

        int sum = a + b;


        try {
            assert sum > 0 : method();
        } catch (AssertionError e) {
            String message = e.getMessage();

            System.out.println("Message: " + message);
        }
    }

    private static String method() {
        System.out.println("executed");
        return "hello";
    }
}
