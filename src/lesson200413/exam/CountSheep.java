package lesson200413.exam;

import java.util.concurrent.*;

public class CountSheep extends RecursiveAction {
    static int[] sheep = new int[]{1, 2, 3, 4};
    final int start;
    final int end;
    static int count = 0;

    public CountSheep(int start, int end) {
        this.start = start;
        this.end = end;
    }

    public void compute() {
        if (end - start < 2) {
            count += sheep[start];
            return;
        } else {

            // 0 ... 500 ... 1000
            // 500 ... 750 ... 1000
            int middle = start + (end - start) / 2;
//            int middle = (end - start) / 2;
            invokeAll(new CountSheep(start, middle),
                    new CountSheep(middle, end));
        }
    }

    public static void main(String[] night) {
        ForkJoinPool pool = new ForkJoinPool();
        CountSheep action = new CountSheep(0, sheep.length);
        pool.invoke(action);
        pool.shutdown();
        System.out.print(action.count);
    }
}
