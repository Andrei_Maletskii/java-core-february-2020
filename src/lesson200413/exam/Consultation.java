package lesson200413.exam;

import java.time.*;
import java.time.format.DateTimeFormatter;

public class Consultation {

    public static void main(String[] args) {
        LocalDate localDate = LocalDate.of (2017,3,12);
        LocalTime localTime = LocalTime.of(1,0);
        ZoneId zoneId = ZoneId.of ("America/New_York");
        ZonedDateTime z = ZonedDateTime.of (localDate,localTime,zoneId);
        System.out.println(z);
//        System.out.println(DateTimeFormatter.ISO_DATE.format(z));
        Duration duration = Duration.ofHours (3);
        ZonedDateTime later = z.plus (duration);
        System.out.println (later);
//        System.out.println (DateTimeFormatter.ISO_DATE.format(later));
    }
}
