package lesson200303;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

public class TryExamples {

    public static void main(String[] args) throws FileNotFoundException {
        new PrintWriter("text.txt");

        PrintWriter printWriter = null;
        try {
             printWriter = new PrintWriter("text.txt");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (printWriter != null) {
                printWriter.close();
            }
        }

        try (PrintWriter pw = new PrintWriter("text.txt")) {
           pw.println();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }



        try (MyCloseable mc = new MyCloseable()) {

        } catch (Exception e) {
            e.printStackTrace();
        }

        MyCloseable mc = null;
        try {
            mc = new MyCloseable();
            mc.method();
        } catch (IOException e) {

        } finally {
            try {
                mc.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}

class MyCloseable implements AutoCloseable {

    public void method() throws IOException {

    }

    @Override
    public void close() throws Exception {

    }
}
