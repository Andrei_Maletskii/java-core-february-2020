package lesson200303;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.SortedMap;

public class OutputStreamExample {

    public static void main(String[] args) throws IOException {
        SortedMap<String, Charset> stringCharsetSortedMap = Charset.availableCharsets();

        System.out.println(stringCharsetSortedMap);

        PrintWriter pw = new PrintWriter("output.txt");
        PrintWriter pw1 = new PrintWriter("output16LE.txt", StandardCharsets.UTF_16LE);
        PrintWriter pw2 = new PrintWriter("output16BE.txt", StandardCharsets.UTF_16BE);

        pw.println("hello");
        pw1.println("hello");
        pw2.println("hello");

        pw.close();
        pw1.close();
        pw2.close();
    }
}
