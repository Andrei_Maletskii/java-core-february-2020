package lesson200303;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PipedReader;
import java.io.PipedWriter;

public class PipedExamples {

    public static void main(String[] args) throws IOException {
        PipedWriter pw = new PipedWriter();

        PipedReader pr = new PipedReader();

        pw.connect(pr);

        pw.write("hello");

        pw.flush();
        pw.close();

        BufferedReader br = new BufferedReader(pr);

        br.lines().forEach(System.out::println);

        pr.close();

    }
}
