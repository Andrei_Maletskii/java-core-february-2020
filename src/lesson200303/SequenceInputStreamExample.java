package lesson200303;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.SequenceInputStream;

public class SequenceInputStreamExample {

    public static void main(String[] args) throws IOException {
        FileInputStream fis1 = new FileInputStream("output16BE.txt");
        FileInputStream fis2 = new FileInputStream("output16LE.txt");

        SequenceInputStream sis1 = new SequenceInputStream(fis1, fis2);
        SequenceInputStream sis2 = new SequenceInputStream(fis1, fis2);

        SequenceInputStream sis = new SequenceInputStream(sis1, sis2);

        try {
            sis.read();
        } catch (IOException e) {
            e.printStackTrace();
        }

        sis.close();
    }
}
