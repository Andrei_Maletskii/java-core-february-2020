package lesson200303;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

public class BAISExample {

    public static void main(String[] args) {
        byte[] buffer = new byte[] {-128, 1, 2, 3, 127};

        ByteArrayInputStream bais = new ByteArrayInputStream(buffer);

        int read = bais.read();

        System.out.println(read);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        baos.write(10);
        baos.write(15);
        baos.write(20);

        byte[] bytes = baos.toByteArray();

    }
}
