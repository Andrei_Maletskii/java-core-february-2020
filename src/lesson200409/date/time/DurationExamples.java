package lesson200409.date.time;

import lesson200305.Fibonacci;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;

public class DurationExamples {

    public static void main(String[] args) {
        Duration estimated = Duration.between(
                Instant.now().plusSeconds(3),
                Instant.now().minusSeconds(3)
        );

        Duration tenDays = Duration.of(10, ChronoUnit.DAYS);

        System.out.println(tenDays);

        System.out.println(estimated);

        Duration hours241 = tenDays.plusMinutes(60);

        System.out.println(hours241);

        estimated.isNegative();
        estimated.isZero();

        estimateOldStyle();
        estimateNewStyle();

        Duration between = Duration.between(
                LocalTime.now().minusHours(1),
                LocalTime.now().plusHours(1)
        );
    }

    private static void estimateOldStyle() {
        long start = System.currentTimeMillis();
        targetMethod();
        long stop = System.currentTimeMillis();
        System.out.println(stop - start);
    }

    private static void estimateNewStyle() {
        Instant start = Instant.now();
        targetMethod();
        Instant stop = Instant.now();
        System.out.println(Duration.between(start, stop));
    }

    private static void targetMethod() {
        int result = Fibonacci.fibSlow(10);
        System.out.println(result);
    }
}
