package lesson200409.date.time;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;

public class DateTimeFormatterExamples {

    public static void main(String[] args) {
//        DateTimeFormatter formatter = new DateTimeFormatter();

        DateTimeFormatter isoDate = DateTimeFormatter.ISO_DATE;
        DateTimeFormatter rfc1123DateTime = DateTimeFormatter.RFC_1123_DATE_TIME;

        String format = isoDate.format(LocalDate.now());

        System.out.println(format);

        String format1 = rfc1123DateTime.format(ZonedDateTime.now());

        System.out.println(format1);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MMM-dd");
        String result = formatter.format(LocalDate.now());

        System.out.println(result);

        TemporalAccessor parse = formatter.parse("2019-Dec-25");

        System.out.println(parse);

        LocalDate parse1 = LocalDate.parse("2020-03-17");

        System.out.println(parse1);

        LocalDate parse2 = LocalDate.parse("2020-Mar-17", formatter);

        System.out.println(parse2);
    }
}
