package lesson200409.date.time;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

public class DateTimeFormatterExam {

    public static void main(String[] args) {
        LocalTime localTime = LocalTime.of (0,0);
        System.out.println(localTime);
        DateTimeFormatter f = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT);
        f.format (localTime);
    }
}
