package lesson200409.date.time;

import java.time.Period;

public class PeriodExamples {

    public static void main(String[] args) {
        Period threeDays = Period.ofDays(3);

        System.out.println(threeDays);

        Period period = threeDays.plusMonths(40);

        System.out.println(period);

        Period normalized = period.normalized();

        System.out.println(normalized);

        Period period1 = Period.of(1, 10, 13);

        Period newWeeksDays = Period.ofWeeks(3);

        Period twoDays = newWeeksDays.ofDays(2);

        Period period2 = Period.ofWeeks(3).ofDays(2);

        System.out.println(period2);

        System.out.println(newWeeksDays);
        System.out.println(twoDays);
    }
}
