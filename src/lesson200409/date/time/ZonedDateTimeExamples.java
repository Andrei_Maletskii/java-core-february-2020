package lesson200409.date.time;

import java.time.*;
import java.util.Set;

public class ZonedDateTimeExamples {

    public static void main(String[] args) {
        ZonedDateTime now = ZonedDateTime.now();

        System.out.println(now);

        ZonedDateTime.now(ZoneId.systemDefault());

        ZonedDateTime now1 = ZonedDateTime.of(
                LocalDate.now(),
                LocalTime.now(),
                ZoneId.systemDefault()
        );

        ZonedDateTime now2 = ZonedDateTime.of(
                LocalDateTime.now(),
                ZoneId.systemDefault()
        );

        ZoneId zone = now2.getZone();

        Set<String> availableZoneIds = ZoneId.getAvailableZoneIds();

        System.out.println(availableZoneIds);

        now2.toLocalDate();
        now2.toLocalTime();
        now2.toLocalDateTime();

        ZoneId cuiaba = ZoneId.of("America/Cuiaba");

        ZonedDateTime cuiabaTime = ZonedDateTime.of(
                LocalDateTime.now(),
                cuiaba
        );
    }
}
