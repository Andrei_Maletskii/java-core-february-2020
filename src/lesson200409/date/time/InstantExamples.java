package lesson200409.date.time;

import java.time.Instant;
import java.util.Date;

public class InstantExamples {

    public static void main(String[] args) {
        Instant now = Instant.now();

        System.out.println(now);

        Instant now2 = Instant.ofEpochMilli(new Date().getTime());

        System.out.println(now2);

        Instant nowPlus1Second = now.plusMillis(1000);
        Instant nowMinus1Second = now.minusSeconds(1);

        System.out.println(now.isAfter(nowMinus1Second));
        System.out.println(now.isBefore(nowPlus1Second));
    }
}
