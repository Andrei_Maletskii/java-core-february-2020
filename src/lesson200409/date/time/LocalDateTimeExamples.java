package lesson200409.date.time;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;

public class LocalDateTimeExamples {
    public static void main(String[] args) {
        LocalDateTime now = LocalDateTime.now();
        System.out.println(now);

        LocalDateTime now1 = LocalDateTime.of(2020, Month.APRIL, 9, 20, 52, 10);

        LocalDateTime now2 = LocalDateTime.of(
                LocalDate.now(),
                LocalTime.now()
        );

        LocalDateTime plus3DaysMidnight = LocalDateTime.of(
                LocalDate.now().plusDays(3),
                LocalTime.MIDNIGHT
        );

        LocalDateTime instance = LocalDateTime.now();
        LocalDate localDate = instance.toLocalDate();
        LocalTime localTime = instance.toLocalTime();

        LocalDateTime localDateTime = instance.plusHours(3);

        LocalDateTime localDateTime1 = localDateTime.minusDays(3);
    }

}
