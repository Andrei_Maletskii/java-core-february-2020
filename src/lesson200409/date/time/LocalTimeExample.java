package lesson200409.date.time;

import java.time.Instant;
import java.time.LocalTime;
import java.time.ZoneId;

public class LocalTimeExample {

    public static void main(String[] args) {
        LocalTime now = LocalTime.now();

        LocalTime time = LocalTime.of(13, 52, 10);

        LocalTime localTime = LocalTime.ofInstant(Instant.now(), ZoneId.systemDefault());

        LocalTime midnight = LocalTime.MIDNIGHT;

        LocalTime minus10HoursFromNow = LocalTime.now().minusHours(10);
    }
}
