package lesson200409.date.time;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.temporal.ChronoUnit;

public class LocalDateExamples {

    public static void main(String[] args) {
//        LocalDate now = new LocalDate();
        LocalDate now = LocalDate.now();
        LocalDate yesterday = LocalDate.of(2020, Month.APRIL, 8);
        LocalDate yesterday1 = LocalDate.of(2020, 4, 8);

        System.out.println(yesterday);
        System.out.println(yesterday1);

        LocalDate epochStart = LocalDate.ofEpochDay(0);

        System.out.println(epochStart);

        LocalDate plus = LocalDate.now().plus(10, ChronoUnit.DAYS);
        System.out.println(plus);
//        LocalDate.now().minusYears()

        LocalDate now1 = LocalDate.from(LocalTime.now());

        LocalTime now2 = LocalTime.from(LocalDate.now());

        LocalTime.from(LocalDateTime.now());
        LocalDate.from(LocalDateTime.now());

        System.out.println(now1);
    }
}
