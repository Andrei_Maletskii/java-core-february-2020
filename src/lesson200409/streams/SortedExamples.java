package lesson200409.streams;

import lesson200407.stream.persons.Car;
import lesson200407.stream.persons.MapAndCollectExamples;
import lesson200407.stream.persons.Person;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static lesson200407.stream.persons.MapAndCollectExamples.getDefaultPersonList;

public class SortedExamples {

    public static void main(String[] args) {
        List<Person> personList = getDefaultPersonList();

        Collections.sort(personList, Comparator.comparing(Person::getName));
        personList.sort((p1, p2) -> p1.getName().compareTo(p2.getName()));

        List<Person> sortedPersons = personList.stream()
                .sorted(Comparator.comparing(Person::getName))
                .collect(Collectors.toList());

        List<Person> personsSortedByCarBrand = personList.stream()
                .sorted(
                        Comparator.comparing(
                                Person::getCar,
                                Comparator.comparing(Car::getBrand)
                        )
                )
                .collect(Collectors.toList());
    }
}
