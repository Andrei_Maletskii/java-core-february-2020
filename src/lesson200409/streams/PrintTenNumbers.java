package lesson200409.streams;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PrintTenNumbers {

    public static void main(String[] args) {
        List<Integer> result = Stream.of(1, 2, 3, 4, 5)
                .map(i -> i * 2)
                .filter(i -> i > 5).collect(Collectors.toList());

        printTenNumbers(true);
        System.out.println("-----");
        printTenNumbers(false);
    }

    private static void printTenNumbers(boolean isEven) {
        Stream<Integer> stream = Stream.iterate(0, i -> ++i);

        if (isEven) {
            stream = stream.filter(i -> i % 2 == 0);
        }

        String result = stream.limit(10)
                .map(Object::toString)
                .collect(Collectors.joining(","));

        System.out.println(result);
    }
}
