package lesson200409.streams;

import lesson200407.stream.persons.Person;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static lesson200407.stream.persons.MapAndCollectExamples.getDefaultPersonList;

public class ReduceExamples {

    public static void main(String[] args) {
        Optional<Integer> sum = Stream.of(1, 2, 3, 4, 5).reduce(Integer::sum);

        sum.ifPresent(System.out::println);

        List<Person> personList = getDefaultPersonList();

        Optional<Person> max = personList.stream().reduce((p1, p2) -> {
            int result = p1.getName().compareTo(p2.getName());

            return result > 0 ? p2 : p1;
        });

        max.ifPresent(System.out::println);

        Stream<Integer> empty = Stream.empty();

        Integer sum1 = Stream.of(1, 2, 3, 4, 5).reduce(0, Integer::sum);
        Integer sum2 = Stream.<Integer>empty().reduce(0, Integer::sum);

        Integer animalsCount = personList.stream()
                .reduce(
                        0,
                        (partialCount, person) -> partialCount + person.getAnimals().size(),
                        Integer::sum
                );

        System.out.println(animalsCount);

        Integer personCount = personList.stream().reduce(
                0,
                (partialCount, person) -> partialCount + 1,
                Integer::sum
        );

        System.out.println(personCount);

    }
}
