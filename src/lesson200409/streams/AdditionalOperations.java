package lesson200409.streams;

import lesson200407.stream.persons.MapAndCollectExamples;
import lesson200407.stream.persons.Person;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AdditionalOperations {

    public static void main(String[] args) {
        long count = getDefaultStream().filter(i -> i % 2 == 0)
                .peek(System.out::println)
                .count();
        System.out.println("count = " + count);

        Optional<Integer> max1 = getDefaultStream().max(Comparator.naturalOrder());
        Optional<Integer> max2 = getDefaultStream().max(Comparator.comparingInt(i -> i));
        Optional<Integer> max3 = getDefaultStream().min(Comparator.reverseOrder());

        Optional<Person> maxByAnimalCount = MapAndCollectExamples.getDefaultPersonList()
                .stream()
                .max(
                        Comparator.comparingInt(
                                p -> p.getAnimals().size()
                        )
                );

        Optional<Integer> first = getDefaultStream().findFirst();
        Optional<Integer> any = getDefaultStream().findAny();

        System.out.println("first = " + first.get());
        System.out.println("any = " + any.get());

        boolean allMatch = getDefaultStream().allMatch(i -> i > -1);
        boolean anyMatch = getDefaultStream().anyMatch(i -> i >= 9);
        boolean noneMatch = getDefaultStream().noneMatch(i -> i > 1000);

        System.out.println("allMatch = " + allMatch);
        System.out.println("anyMatch = " + anyMatch);
        System.out.println("noneMatch = " + noneMatch);

//        List<Integer> dropWhile = getDefaultStream().dropWhile(i -> i % 3 == 0).collect(Collectors.toList());
//        System.out.println(dropWhile);
//        getDefaultStream().takeWhile()

        Stream<Integer> parallel = Stream.iterate(0, i -> ++i).limit(100).parallel();

        System.out.println(parallel.isParallel());

//        parallel.sequential()

        Integer reduce = parallel.reduce(
                0,
                (i1, i2) -> {
                    int sum = i1 + i2;
                    System.out.println(Thread.currentThread().getName() + " partial sum = " + sum);
                    return sum;
                }
        );

//        ForkJoinPool.commonPool();


        System.out.println(reduce);
//        getDefaultStream().forEachOrdered();

        System.out.println("----");
        Stream.iterate(1, i -> ++i).limit(100).parallel().forEach(System.out::println);
        Stream.iterate(1, i -> ++i).limit(100).parallel().forEachOrdered(System.out::println);

        System.out.println("----");

        List<Integer> takeWhileResult = getDefaultStream()
                .parallel()
                .takeWhile(i -> i < 5)
                .collect(Collectors.toList());
        System.out.println(takeWhileResult);

        List<Integer> dropWhileResult = getDefaultStream()
                .parallel()
                .dropWhile(i -> i < 5)
                .collect(Collectors.toList());
        System.out.println(dropWhileResult);

        List<Integer> result = getDefaultStream()
                .dropWhile(i -> i % 3 == 0)
                .collect(Collectors.toList());

        System.out.println(result);

    }

    private static Stream<Integer> getDefaultStream() {
        return Stream.iterate(0, i -> ++i).limit(10);
    }
}
