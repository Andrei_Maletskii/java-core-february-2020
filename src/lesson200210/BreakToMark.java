package lesson200210;

public class BreakToMark {

    public static void main(String[] args) {

        Outer:
        for (int i = 0; i < 10; i++) {
            Inner:
            for (int j = 0; j < 10; j++) {
                if (true) {
                    break Outer;
                }
            }
        }

//        while (true) {
//
//        }

        Integer.parseInt("100000000000");

//        for (;;) {
//
//        }
    }
}
