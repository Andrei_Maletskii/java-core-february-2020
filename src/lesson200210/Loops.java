package lesson200210;

import java.util.Iterator;

public class Loops {
    private static class MyInnerClass {

    }

    public static void main(String[] args) {
        int[] array = {1, 2, 3};

        for (int i = 0; i < array.length; i++) {
            if (array[i] == 1) {
                continue;
            }
            System.out.println(array[i]);
        }

        int j = 10;
        while (true) {
            j++;
            if (j > 100) {
                break;
            }
        }

        int k = 10;
        do {


        } while (k > 0);

        for (int i : array) {

        }

        MyList integers = new MyList();

        for (Integer integer : integers) {

        }
    }
}

class MyList implements Iterable<Integer> {

    @Override
    public Iterator<Integer> iterator() {
        return null;
    }
}
