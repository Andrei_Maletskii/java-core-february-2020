package lesson200210;

public class StringPool {

    public static void main(String[] args) {
        String s1 = "Hello";
        String s2 = "Hello";
        String s3 = new String("Hello");
        String s4 = s3.intern();



        System.out.println(s1 == s2);
        System.out.println(s3 == s2);
        System.out.println(s4 == s2);
    }
}
