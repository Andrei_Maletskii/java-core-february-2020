package lesson200210;

import java.time.LocalDate;
import java.util.Date;

public class ArrayStore {

    private LocalDate[] data = {LocalDate.now()};

    public LocalDate[] getData() {
        return data;
    }
}
