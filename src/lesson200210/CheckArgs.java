package lesson200210;

public class CheckArgs {

    public static void main(String[] args) {
        checkPercents(100, 100);
        checkPercents(-1, -1);
    }

    public static void checkPercents(int percent, Integer integer) {
        if (percent < 0 || percent > 100) {
            throw new IllegalArgumentException();
        }

        if (integer == null) {
            throw new IllegalArgumentException();
        }

    }
}
