package lesson200210;

public class ArraysExamples {

    public static void main(String[] args) {
        int[] data = new int[10];

        for (int i = 0; i < data.length; i++) {
            System.out.println(data[i]);
        }

        Integer[] refData = new Integer[10];

        for (int i = 0; i < refData.length; i++) {
            System.out.println(refData[i]);
        }

        int[] initData = new int[] {1, 2, 3, 4, 5};
        int[] initData2 = {1, 2, 3, 4, 5};

        initData = new int[] {4, 5};
//        initData2 = {4, 5}; ERROR

        initData = new int[] {10, 10};


        int[] intData1;
        int intData2[]; // not recommended

        int[][] matrix = {
                {1,2},
                {3,4},
                {5,6}
        };

        matrix = new int[][] {
                {7,8},
                {1,2},
                {3,4}
        };

        int matrix2[][][];

        Number[] numbers = {10, 10.4, 11.5f};

        Object[] objects = {"hello", 10, 11.5f};


    }
}
