package lesson200210;

public class IntChanger {
    private int value = 0;

    void add(int number) {
        value += number;
    }

    public static void main(String[] args) {
        final IntChanger intChanger = new IntChanger();

//        intChanger = new IntChanger();

        intChanger.value = 10;
        intChanger.add(10);




        final IntChanger[] changers = {new IntChanger(), new IntChanger()};

        changers[0].add(10);
        changers[1].add(20);

//        changers = new IntChanger[5]; ERROR


    }
}
