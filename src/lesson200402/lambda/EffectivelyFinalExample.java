package lesson200402.lambda;

public class EffectivelyFinalExample {
    public static void main(String[] args) {
        new X().method(5);
    }

}

class X {
    int instanceInt;
    static int staticInt;

    public Runnable method(int arg) {
        // local int arg = arg
        int localInt = 10;

        Runnable runnable = new Runnable() {
            int instanceInt = 0;

            @Override
            public void run() {
                // local int arg = arg
//                System.out.println(arg);
                System.out.println(this.instanceInt);
                System.out.println(X.this.instanceInt);
                System.out.println(X.staticInt);
//                System.out.println(localInt);
            }
        };

        Runnable runnable1 = () -> {
//            System.out.println(arg);
            System.out.println(this.instanceInt);
            System.out.println(X.this.instanceInt);
            System.out.println(X.staticInt);
//            System.out.println(localInt);
        };

        arg++;
        instanceInt++;
        staticInt++;
        localInt++;

        return runnable;
    }
}
