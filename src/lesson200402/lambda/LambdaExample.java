package lesson200402.lambda;

public class LambdaExample {
    public static void main(String[] args) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                System.out.println("hello");
            }
        };

        Runnable runnable1 = () -> System.out.println("hello");

        process(new Runnable() {
            @Override
            public void run() {
                System.out.println("hello");
            }
        });

        process(() -> {
            System.out.println("hello");
        });
    }

    public static void process(Runnable runnable) {

    }
}
