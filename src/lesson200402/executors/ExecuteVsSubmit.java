package lesson200402.executors;

import lesson200305.Fibonacci;

import java.math.BigInteger;
import java.util.concurrent.*;

public class ExecuteVsSubmit {

    public static void main(String[] args) {
//        Executor executor = Executors.newSingleThreadExecutor();
        ExecutorService executorService = Executors.newSingleThreadExecutor();

//        executorService.execute(new Runnable() {
//            @Override
//            public void run() {
//                System.out.println("working");
//            }
//        });

        Future<?> submit = executorService.submit(new Runnable() {
            @Override
            public void run() {
                System.out.println("Working");
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        System.out.println("working on other things");

        try {
            submit.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        System.out.println("got results");

        Future<Integer> fib30Future = executorService.submit(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                return Fibonacci.fibSlow(30);
            }
        });

        int fib33 = Fibonacci.fibSlow(33);

        try {
            Integer fib30 = fib30Future.get();
            System.out.println(fib33);
            System.out.println(fib30);

            System.out.println(fib33 + fib30);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }


        executorService.shutdown();
    }

    public static void method(Executor executor) {

    }
}
