package lesson200402.executors;

import java.util.concurrent.*;

public class OwnExecutorOptions {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService myES = Executors.newSingleThreadExecutor(new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r) {
                Thread thread = new Thread(r);

                thread.setName("myOwnThreadFromPool");
                thread.setDaemon(true);

                return thread;
            }
        });

        Future<?> hello = myES.submit(new Runnable() {
            @Override
            public void run() {
                System.out.println("hello");
            }
        });

        hello.get();

//        ExecutorService executorService = new ThreadPoolExecutor(); custom ES


    }
}
