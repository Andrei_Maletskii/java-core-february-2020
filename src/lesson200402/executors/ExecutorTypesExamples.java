package lesson200402.executors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;

public class ExecutorTypesExamples {

    public static void main(String[] args) {
//        ExecutorService single = Executors.newSingleThreadExecutor();

        ExecutorService fixed = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

        List<Future<Integer>> futures = new ArrayList<>();
        for (int i = 1; i <= 1000; i++) {
            final int j = i;
            Future<Integer> ipow3Future = fixed.submit(new Callable<Integer>() {
                @Override
                public Integer call() throws Exception {
                    return j * j * j;
                }
            });

            futures.add(ipow3Future);
        }

        long sum = 0;
        for (Future<Integer> future : futures) {
            try {
                Integer ipow3 = future.get();
                sum += ipow3;
                System.out.println(ipow3);
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }

        System.out.println(sum);

        fixed.shutdown();

        ExecutorService cached = Executors.newCachedThreadPool();

        for (int i = 0; i < 100; i++) {
            cached.submit(new Runnable() {
                @Override
                public void run() {
                    System.out.println("hello " + Thread.currentThread().getName());
                }
            });
        }

        cached.shutdown();

        ScheduledExecutorService scheduled = Executors.newScheduledThreadPool(4);

        System.out.println("sending task to scheduled executor");
        ScheduledFuture<?> schedule = scheduled.schedule(new Runnable() {
            @Override
            public void run() {
                System.out.println("Executed after 10 seconds");
            }
        }, 10, TimeUnit.SECONDS);



        scheduled.shutdown();

        long convertResult = TimeUnit.SECONDS.convert(10, TimeUnit.HOURS);
        try {
            TimeUnit.SECONDS.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
