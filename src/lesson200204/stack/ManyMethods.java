package lesson200204.stack;

public class ManyMethods {

    int i = 5;
    Object field = new Object();

    public void one() {
        two();
    }

    private void two() {
        three();
    }

    private void three() {
        four();
    }

    private void four() {
        System.out.println("hello");
    }


    public static void main(String[] args) {
        ManyMethods a = new ManyMethods();
        int j = 5;
        Object local = new Object();


        a.one();
    }
}
