package lesson200331.producer.consumer;

import java.util.Random;

public class Producer implements Runnable {

    private int count;
    private int bound;
    private MyBlockingLinkedQueue queue;

    public Producer(int count, int bound, MyBlockingLinkedQueue queue) {
        this.count = count;
        this.bound = bound;
        this.queue = queue;
    }

    @Override
    public void run() {
        Random random = new Random();

        for (int i = 0; i < count; i++) {
            try {
                queue.put(random.nextInt(bound));
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
