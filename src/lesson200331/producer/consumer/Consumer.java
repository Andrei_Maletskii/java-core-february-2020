package lesson200331.producer.consumer;

import java.util.ArrayList;
import java.util.List;

public class Consumer implements Runnable {

    private List<Integer> store = new ArrayList<>();
    private MyBlockingLinkedQueue queue;

    public Consumer(MyBlockingLinkedQueue queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        while (true) {
            Integer integer = null;
            try {
                integer = queue.take();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

            if (integer.equals(Integer.MIN_VALUE)) {
                return;
            }

            store.add(integer);
        }
    }

    public List<Integer> getStore() {
        return store;
    }
}
