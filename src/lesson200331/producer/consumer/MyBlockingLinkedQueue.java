package lesson200331.producer.consumer;

import java.util.LinkedList;
import java.util.Queue;

public class MyBlockingLinkedQueue {

    private Queue<Integer> internalQueue = new LinkedList<>();
    private int capacity;

    public MyBlockingLinkedQueue(int capacity) {
        this.capacity = capacity;
    }

    // used by producer
    public synchronized void put(Integer i) throws InterruptedException {
        while (internalQueue.size() >= capacity) {
            System.out.println(Thread.currentThread().getName() + " The queue is full, waiting...");
            this.wait();
            System.out.println(Thread.currentThread().getName() + " The queue is not full, go for it...");
        }

        internalQueue.offer(i);
        this.notifyAll();
    }

    // used by consumer
    public synchronized Integer take() throws InterruptedException {
        while (internalQueue.isEmpty()) {
            System.out.println(Thread.currentThread().getName() + " The queue is empty, waiting...");
            this.wait();
            System.out.println(Thread.currentThread().getName() + " The queue is not empty, go for it...");
        }

        Integer integer = internalQueue.poll();
        this.notifyAll();
        return integer;
    }

    public static void main(String[] args) throws InterruptedException {
        MyBlockingLinkedQueue queue = new MyBlockingLinkedQueue(1);

        Consumer consumerRunnable1 = new Consumer(queue);
        Consumer consumerRunnable2 = new Consumer(queue);
        Producer producerRunnable1 = new Producer(1000, 1000, queue);
        Producer producerRunnable2 = new Producer(1000, 1000, queue);

        Thread consumer1 = new Thread(consumerRunnable1);
        Thread consumer2 = new Thread(consumerRunnable2);
        Thread producer1 = new Thread(producerRunnable1);
        Thread producer2 = new Thread(producerRunnable2);

        consumer1.setName("consumer1");
        consumer2.setName("consumer2");
        producer1.setName("producer1");
        producer2.setName("producer2");

        consumer1.start();
        consumer2.start();
        producer1.start();
        producer2.start();

        producer1.join();
        producer2.join();

        queue.put(Integer.MIN_VALUE);
        queue.put(Integer.MIN_VALUE);

        consumer1.join();
        consumer2.join();

        System.out.println("Store size 1 = " + consumerRunnable1.getStore().size());
        System.out.println("Store size 2 = " + consumerRunnable2.getStore().size());

    }
}
