package lesson200331;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SynchronizedExample {

    public static void main(String[] args) {
        List<Object> objects = Collections.synchronizedList(new ArrayList<>());
    }
}

class MySyncArrayList extends ArrayList {
    @Override
    public synchronized boolean add(Object o) {
        return super.add(o);
    }
    // etc
}
