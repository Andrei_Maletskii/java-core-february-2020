package lesson200331.deadlock.and.starvation;

public class DeadlockExample {

    public static final int N = 100;

    public static void main(String[] args) {
        Object fork = new Object();
        Object knife = new Object();

        FirstPhilosopher first = new FirstPhilosopher(fork, knife);
        SecondPhilosopher second = new SecondPhilosopher(fork, knife);

        first.start();
        second.start();
    }
}

abstract class AbstractPhilosopher extends Thread {
    protected Object fork;
    protected Object knife;

    public AbstractPhilosopher(Object fork, Object knife) {
        this.fork = fork;
        this.knife = knife;
    }
}

class FirstPhilosopher extends AbstractPhilosopher {
    public FirstPhilosopher(Object fork, Object knife) {
        super(fork, knife);
    }

    @Override
    public void run() {
        for (int i = 0; i < DeadlockExample.N; i++) {
            synchronized (fork) {
                synchronized (knife) {
                    System.out.println("First philosopher eating...");
                }
            }
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println("First philosopher finished eating...");
    }
}

class SecondPhilosopher extends AbstractPhilosopher {
    public SecondPhilosopher(Object fork, Object knife) {
        super(fork, knife);
    }

    @Override
    public void run() {
        for (int i = 0; i < DeadlockExample.N; i++) {
            synchronized (fork) {
                synchronized (knife) {
                    System.out.println("Second philosopher eating...");
                }
            }
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println("Second philosopher finished eating...");
    }
}
