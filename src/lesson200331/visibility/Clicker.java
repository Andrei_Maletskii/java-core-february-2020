package lesson200331.visibility;

public class Clicker extends Thread {

    int click = 0;
    private volatile boolean running = true;
    // 1. Make volatile
    // 2. make all read and write ops with running synchronized

    @Override
    public void run() {
        while (running) {
            click++;
        }
    }

    public void stopClick() {
        running = false;
    }

    public boolean isRunning() {
        return running;
    }

    public static void main(String[] args) throws InterruptedException {
        Clicker clicker = new Clicker();
        clicker.setName("clicker-0");
        clicker.start();

        Thread.sleep(500);

        clicker.stopClick();

        Thread.sleep(500);

        System.out.println(clicker.isRunning());
    }
}
