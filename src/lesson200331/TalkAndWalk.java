package lesson200331;

import lesson200330.priority.TalkingThread;

public class TalkAndWalk {
    public static void main(String[] args) throws InterruptedException {
        TalkingThread talkingRunnable = new TalkingThread();

        Thread thread = new Thread(talkingRunnable);
        thread.start();

        Thread.sleep(1500);
        System.out.println("Suspend Talking...");
        thread.suspend();
        Thread.sleep(1500);
        System.out.println("Resume talking...");
        thread.resume();
        Thread.sleep(1500);
        System.out.println("Stop talking");
        thread.stop();

    }
}
