package lesson200302;

public class StringBuilderExample {

    public static void main(String[] args) {
        String s1 = "Hel";
        String s2 = "lo";

        StringBuilder s3 = new StringBuilder(s1);

        for (int i = 0; i < 100; i++) {
            s3.append(s2);
        }

        String s5 = s3.toString();

        StringBuilder sb = new StringBuilder(s1);
        sb.append(s2);
        sb.append(4);
        sb.append("a");
        String s4 = sb.toString();

        StringBuilder sb1 = new StringBuilder();
        sb1.append(s2).append(s2).append(s2);
        String s = sb1.toString();
    }
}
