package lesson200302;

import java.io.FileNotFoundException;
import java.util.Formatter;

public class FormatterSimpleExample {

    public static void main(String[] args) throws FileNotFoundException {
        String formattedString = String.format("%2$s - %2$s%% %s of progress", "Loading", "67");

        System.out.println(formattedString);

        Formatter formatter = new Formatter("text.txt");

        formatter.format("%s - %d%% of progress", "Loading", 67);

        formatter.flush();

        formatter.close();
    }
}
