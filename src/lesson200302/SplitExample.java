package lesson200302;

public class SplitExample {

    public static void main(String[] args) {
        String str = "Hello4world6car7pot";

        String[] split = str.split("\\d");

        for (String s : split) {
            System.out.println(s);
        }
    }
}
