package lesson200302;

import java.time.ZonedDateTime;

public class LiteralPoolExamples {

    public static final String HEL = "Hel";
    public static final String LO = "lo";
    public static final String HELLO = HEL + LO;

    public static void main(String[] args) {
        String hello = "Hello";

        String lo = args[0];

        String hello1 = "Hel" + "lo";

        System.out.println(hello == hello1);
        System.out.println(hello == "Hel" + lo);
        System.out.println(hello == HELLO);
        System.out.println(hello == HEL + LO);




    }
}
