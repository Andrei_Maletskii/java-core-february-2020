package lesson200414;

import java.util.Comparator;
import java.util.stream.Stream;

public class StreamExample {

    public static void main(String[] args) {
        Stream.of(
                "over the river",
                "through the woods",
                "to grandmother's house we go"
        )
                .filter(n -> n.startsWith("t"))
//                .sorted(Comparator.reverseOrder())
//                .sorted(Comparator::reverseOrder)
                .sorted(String::compareTo)
                .findFirst()
                .ifPresent(System.out::println);

        Comparator<String> tComparator = Comparator.reverseOrder();
    }
}
