package lesson200414;

import java.io.*;
import java.util.Arrays;

public class FileConsoleExamples {

    public static void main(String[] args) throws IOException {
        File file = new File("../JavaFeburary2020/output.txt");
        File file1 = new File("./output.txt");
        File file2 = new File("C:\\Users\\Andrei_Maletskii\\IdeaProjects\\JavaFebruary2020");
//        File file2 = new File("/var/log/...");
        System.out.println(file);
        System.out.println(file1);
        System.out.println(file2);

        System.out.println(file1.isFile());
        System.out.println(file2.isDirectory());

        File dir = new File(".git");
        System.out.println(dir.isHidden());

        System.out.println(file.getName());
        System.out.println(file.getAbsolutePath());
        try {
            System.out.println(file.getCanonicalPath());
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(file.isAbsolute());
        System.out.println(file2.isAbsolute());

        File nestedDir = new File("./folder");
        System.out.println(nestedDir.isDirectory());
        System.out.println(nestedDir.exists());
        System.out.println(nestedDir.mkdir());
        System.out.println(nestedDir.exists());
        System.out.println(nestedDir.isDirectory());

        System.out.println("----");
        File file3 = new File("./folder1/folder2/folder3");
        System.out.println(file3.exists());
//        System.out.println(file3.mkdir());
        System.out.println(file3.mkdirs());

        System.out.println("-----");
        File currentDir = new File(".");
        System.out.println(currentDir.isDirectory());
        System.out.println(Arrays.toString(currentDir.list()));
        String[] folders = currentDir.list(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.startsWith("folder");
            }
        });

        System.out.println(Arrays.toString(folders));

        File[] files = currentDir.listFiles();
        System.out.println(Arrays.toString(files));

        File[] dirs = currentDir.listFiles(File::isDirectory);

        System.out.println(Arrays.toString(dirs));

        boolean b1 = file.setWritable(true);
        file.setReadable(true);
        file.setExecutable(true);

        boolean b = file.canRead();
        file.canWrite();
        file.canExecute();

        File file4 = new File("folder");

//        file4.delete();

//        file4.deleteOnExit();

        System.out.println("hello");

        File file5 = new File("file12345.txt");
        file5.createNewFile();
//        file4.renameTo(new File("folder2"));

        System.out.println(file4.getParent());
        System.out.println(file4.getParentFile());

        File folderClone = new File(file4.getCanonicalPath());

        System.out.println(folderClone.getParent());
    }
}
