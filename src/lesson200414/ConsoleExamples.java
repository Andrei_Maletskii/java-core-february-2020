package lesson200414;

import java.io.Console;
import java.io.IOException;

public class ConsoleExamples {

    public static void main(String[] args) throws IOException {
        Console c = System.console();
        final String response = c.readLine("Are you human?");
        System.err.print(response);
    }
}
