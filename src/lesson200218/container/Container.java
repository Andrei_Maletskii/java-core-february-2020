package lesson200218.container;

import java.util.List;

public class Container<T extends Comparable<T>> {

    private T value;

    public Container(T value) {
        this.value = value;
    }

    public T getValue() {
        return value;
    }

    public void process(T other) {
        T value = this.value;
        int result = value.compareTo(other);
    }

    public static void main(String[] args) {
        Container<String> container = new Container<>("str");


        String value = container.getValue();

        Container<Integer> integerContainer = new Container<>(1);

        value.toUpperCase();

        Container container1 = new Container("str");


        Container<String> value2 = new Container<>("str");
        Integer value1 = new Container<Integer>(null).getValue();


        int[][][] matrix;
        List<List<List<Integer>>> list;


        int[][][][] matrix4 = new int[3][][][];

        matrix4[0] = new int[3][][];
        matrix4[1] = new int[3][][];
        matrix4[2] = new int[3][][];

        matrix4[0][0] = new int[3][];
        matrix4[0][1] = new int[3][];
        matrix4[0][2] = new int[3][];




    }
}
