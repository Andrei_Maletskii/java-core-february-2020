package lesson200218.person;

import java.util.Arrays;
import java.util.Comparator;

public class Person {

    private int id;
    private String name;
    private int age;

    public Person(int id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "{" + id + ' ' + name + ' ' + age + '}';
    }

    public static void main(String[] args) {
        Person[] persons = {
                new Person(5, "Ann", 30),
                new Person(1, "Ann", 19),
                new Person(2, "Bob", 30),
                new Person(3, "Tom", 25),
                new Person(4, "Jack", 35)
        };
        Arrays.sort(new int[]{1, 5, 3, 2});

        Arrays.sort(persons, new AgeComparator());

        System.out.println(Arrays.toString(persons));

        Arrays.sort(persons, new NameComparator());

        System.out.println(Arrays.toString(persons));
    }
}

class AgeComparator implements Comparator<Person> {

    @Override
    public int compare(Person o1, Person o2) {
        return o1.getAge() - o2.getAge();
    }
}

class NameComparator implements Comparator<Person> {

    @Override
    public int compare(Person o1, Person o2) {
        return o1.getName().compareTo(o2.getName());
    }
}


