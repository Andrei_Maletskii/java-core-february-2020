package lesson200218;

import lesson200218.container.Container;

import java.util.Comparator;

public class ExtendsExample {

    public static void main(String[] args) {
        Message<A> message = new Message<>(new A());
        Message<B> message1 = new Message<>(new B());
    }
}

class A {

}

class B extends A {

}

class Message<T extends A> {
    private T text;

    public Message(T text) {
        this.text = text;
    }

    public T getText() {
        return text;

    }
}