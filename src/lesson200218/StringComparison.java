package lesson200218;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class StringComparison {

    public static void main(String[] args) {
        String[] strings = {
                "zzzzzzzzzzzz",
                "a",
                "kkkk",
                "bbbbbbbbbbb"
        };

        Arrays.sort(strings, new StringLengthComparator());

        System.out.println(Arrays.toString(strings));



    }
}

class StringLengthComparator implements Comparator<String> {

    @Override
    public int compare(String o1, String o2) {
        return o1.length() - o2.length();
    }
}
