package lesson200218;

import java.util.Arrays;

public class ComparableExamples {

    public static void main(String[] args) {
//        examples();

        sortPersons();
    }

    private static void examples() {
        int[] array = {6, 4, 8, 9, 10, 3};

        Arrays.sort(array);

        System.out.println(Arrays.toString(array));

        MyInteger[] myArray = {
                new MyInteger(6),
                new MyInteger(4),
                new MyInteger(8),
                new MyInteger(9),
                new MyInteger(10),
                new MyInteger(3),

        };

        Arrays.sort(myArray);

        MyInteger five = new MyInteger(5);
        MyInteger ten = new MyInteger(10);

        System.out.println(ten.compareTo(ten));
        System.out.println(ten.compareTo(five));
        System.out.println(five.compareTo(ten));
    }

    private static void sortPersons() {
        Person[] persons = {
                new Person("Mary", 24),
                new Person("Bob", 25),
                new Person("Ann", 20),
                new Person("Bob", 30)
        };

        Arrays.sort(persons);

        System.out.println(Arrays.toString(persons));
    }
}

class MyInteger implements Comparable<MyInteger> {
    int value;
    int order;

    public MyInteger(int value) {
        this.value = value;
        this.order = 0;
    }

    public int getValue() {
        return value * (int) Math.pow(10, order);
    }

    @Override
    public String toString() {
        return "MyInteger{" + value + '}';
    }

    @Override
    public int compareTo(MyInteger other) {
        if (this.value > 0 && other.value < 0) {
            return 1;
        }

        if (this.value < 0 && other.value > 0) {
            return -1;
        }

        if (this.value > 0 && other.value > 0) {
            return this.order - other.order;
        }

        if (this.value < 0 && other.value < 0) {
            return other.order - this.order;
        }

        return this.value - other.value;
    }
}

class Person implements Comparable<Person> {

    private String firstName;
    private int age;

    public Person(String firstName, int age) {
        this.firstName = firstName;
        this.age = age;
    }

    public String getFirstName() {
        return firstName;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "Person{" + firstName + ' ' + age + '}';
    }

    @Override
    public int compareTo(Person that) {
        int result = this.firstName.compareTo(that.firstName);

        if (result != 0) {
            return result;
        }

        return this.age - that.age;
    }
}
