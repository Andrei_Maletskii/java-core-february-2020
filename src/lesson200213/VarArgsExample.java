package lesson200213;

public class VarArgsExample {

    public static void main(String... args) {
//    public static void main(String[] args) {
//    public static void main(String args[]) {
        method();
        method(1,2);
        method(1,2,3);
        method(1,2,3,4,5,6,7,8,9,0);
    }

    private static void method(int... args) {
        for (int i = 0; i < args.length; i++) {
            System.out.print(args[i] + " ");
        }
        System.out.println();
    }

//    private static void method(int[]... args) {
//        for (int i = 0; i < args.length; i++) {
//            System.out.print(args[i] + " ");
//        }
//        System.out.println();
//    }
}
