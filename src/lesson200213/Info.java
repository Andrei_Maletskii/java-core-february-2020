package lesson200213;

public class Info {
    private int i;

    public Info(int i) {
        this.i = i;
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("I'm dying " + i);
    }

    public static void main(String[] args) throws InterruptedException {
        Info info = new Info(-1);
        System.gc();
        for (int i = 0; i < 3_000; i++) {
            info = new Info(i);
            if (i % 10 == 0) {
                System.runFinalization();
            }
        }
    }
}
