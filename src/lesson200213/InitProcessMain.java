package lesson200213;

public class InitProcessMain {
    public static void main(String[] args) {
        InitProcessB b = new InitProcessB();
    }
}

class InitProcessA {
    static {
        System.out.println("static init A (1) uuid=" + InitProcessA.uuid);
    }
    {
        System.out.println("init A (1) id=" + this.id);
    }
    private static int uuid = 11;
    private int id = 5;
    static {
        System.out.println("static init A (2) uuid=" + InitProcessA.uuid);
    }
    {
        System.out.println("init A (2) id=" + this.id);
    }

    public InitProcessA() {
        this.id = 10;
        System.out.println("constructor A id=" + this.id);
    }
}

class InitProcessB extends InitProcessA {
    static {
        System.out.println("static init B");
    }
    {
        System.out.println("init B");
    }
    public InitProcessB() {
        System.out.println("constructor B");
    }
}
