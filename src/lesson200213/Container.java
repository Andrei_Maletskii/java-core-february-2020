package lesson200213;

public class Container {
    private int id;
    private int count;
    private double cf;
    private String str;

    public Container(int id, int count) {
        this.id = id;
        this.count = count;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Container container = (Container) o;

        if (id != container.id) return false;
        if (count != container.count) return false;
        if (Double.compare(container.cf, cf) != 0) return false;
        return str != null ? str.equals(container.str) : container.str == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = id;
        result = 31 * result + count;
        temp = Double.doubleToLongBits(cf);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (str != null ? str.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Container{" +
                "id=" + id +
                ", count=" + count +
                '}';
    }

    public static void main(String[] args) {
        Container container1 = new Container(0, 5);
        Container container2 = new Container(5, 0);

        System.out.println(container1.hashCode());
        System.out.println(container2.hashCode());

        Object object = new Container(1,1);

        Class<?> aClass = object.getClass();

        System.out.println(aClass);

        System.out.println(object);


    }

    public static void method(Object object) {
        Class<?> aClass = object.getClass();

    }
}
