package lesson200213;

public class DefaultConstructor {

    public static void main(String[] args) {
        A a = new A();
        B b = new B();

        b.toString();
        System.out.println(b.method());
    }
}

class A {

    public int value = 10;

    public A() {
    }

    public A(int i) {
        value = i;
    }

    public int method() {
        return 17;
    }
}

class B extends A {
    public int value = 15;

    @Override
    public int method() {
        return super.method() + 3;
    }

    @Override
    public String toString() {
        return "B{" +
                "value=" + super.value +
                ", value=" + this.value +
                '}';
    }
}
