package lesson200213.hospital;

public class Doctor extends Man {

    static {
        System.out.println("static block in Doctor");
    }
    public static void stDoctor() {
        System.out.println("static method in Doctor.");
    }
    public static void stMan() {
        System.out.println("static method stMan in Doctor.");
    }

}
