package lesson200217.auto;

public class Lada extends Vehicle {

    public Lada(int speed) {
        super(speed);
    }

    @Override
    public int move() {
        System.out.println("Car moves");
        return super.move();
    }

    @Override
    public int getJumpingDistance() {
        return 50;
    }
}
