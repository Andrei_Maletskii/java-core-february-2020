package lesson200217.auto;

public abstract class Vehicle implements Moveable {

    private int wheelCount;
    private int speed;

    public Vehicle(int speed) {
        this.speed = speed;
    }

    @Override
    public int move() {
        System.out.println("Base move");
        return speed;
    }
}
