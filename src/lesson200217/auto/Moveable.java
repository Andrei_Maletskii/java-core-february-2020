package lesson200217.auto;

public interface Moveable {

    int move();

    default int jump() {
        sayHello();

        return getJumpingDistance();
    }

    int getJumpingDistance();

    private void sayHello() {
        System.out.println("hello");
    }

    default void rev() {
        System.out.println("REVVV...");
    }
}
