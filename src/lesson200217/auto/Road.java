package lesson200217.auto;

public class Road {

    public static void main(String[] args) {
        moveOnRoad(new Lada(10));
        moveOnRoad(new Motorcycle(15));
        moveOnRoad(new Box());
    }

    private static void moveOnRoad(Moveable moveable) {
        moveable.rev();
        moveable.jump();
        moveable.getJumpingDistance();
        moveable.move();
    }
}
