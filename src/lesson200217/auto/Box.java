package lesson200217.auto;

public class Box implements Moveable {


    @Override
    public int move() {
        System.out.println("I've been kicked");
        return 1;
    }

    @Override
    public int getJumpingDistance() {
        return 0;
    }
}
