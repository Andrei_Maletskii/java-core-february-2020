package lesson200217.auto;

public class Motorcycle extends Vehicle {

    public Motorcycle(int speed) {
        super(speed);
    }

    @Override
    public int move() {
        System.out.println("Motorcycle moves");
        return super.move();
    }

    @Override
    public int getJumpingDistance() {
        return 20;
    }

    @Override
    public int jump() {
        return 0;
    }
}
