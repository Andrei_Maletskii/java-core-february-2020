package lesson200217;

import java.util.Date;

public class Department implements Cloneable {
    private Integer name = 1000;
    private Date date = new Date();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Department that = (Department) o;

        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        return date != null ? date.equals(that.date) : that.date == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (date != null ? date.hashCode() : 0);
        return result;
    }

    @Override
    public Department clone() throws CloneNotSupportedException {
        Department obj = null;

        obj = (Department) super.clone();
//        if (null != this.name) {
//            obj.name = (Integer) this.name.clone(); //ERROR
//        }
        if (null != this.date) {
            obj.date = (Date) this.date.clone();
        }
        return obj;
    }

    public Integer getName() {
        return name;
    }

    public Date getDate() {
        return date;
    }

    public static void main(String[] args) throws CloneNotSupportedException {
        Department departmentOriginal = new Department();
        Department departmentClone = departmentOriginal.clone();
        System.out.println(departmentOriginal == departmentClone);
        System.out.println(departmentOriginal.equals(departmentClone));

        System.out.println(departmentOriginal.getName());
        System.out.println(departmentClone.getName());

        System.out.println(departmentOriginal.getName() == departmentClone.getName());
        System.out.println(departmentOriginal.getDate() == departmentClone.getDate());
        System.out.println(departmentOriginal.getDate().equals(departmentClone.getDate()));
//        System.out.println(departmentClone.getDate());
    }
}

