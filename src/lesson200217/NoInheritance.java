package lesson200217;

import lesson200217.multiple.inheritance.Interface1;
import lesson200217.multiple.inheritance.Interface2;

public final class NoInheritance {


}

class NoInheritance2 {

    private NoInheritance2() {

    }

    public static NoInheritance2 getInstance() {
        return new NoInheritance2();
    }
}

class Child
//        extends NoInheritance2
{

    public Child() {
//        super(); error
        Interface1.method();
    }

}