package lesson200217;

public interface Square {
    public static final double PI = 3.1415926;

    public abstract double square();
}

