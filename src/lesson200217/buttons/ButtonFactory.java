package lesson200217.buttons;

public abstract class ButtonFactory {

    public ButtonFactory() {

    }

    public abstract Button getButton();
}

class MyButtonFactory extends ButtonFactory {

    @Override
    public Button getButton() {
        return null;
    }
}

abstract class MyAbstractButtonFactory extends ButtonFactory {

}
