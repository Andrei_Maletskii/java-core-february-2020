package lesson200217.buttons;

public class CivilButtonFactory extends ButtonFactory {

    @Override
    public GreenButton getButton() {
        return new GreenButton();
    }
}
