package lesson200217.buttons;

import java.util.Collection;
import java.util.Date;

public class Main {

    public static void main(String[] args) {
        CivilButtonFactory cbf = new CivilButtonFactory();
        GreenButton button = cbf.getButton();
//        Button button = cbf.getButton();
        button.press();
        button.brake();

        if (button instanceof Collection) {
            Collection collection = (Collection) button;
        }


        proceed(new CivilButtonFactory());
        proceed(new WarButtonFactory());
        proceed(new NuclearWarButtonFactory());
    }

    private static void proceed(ButtonFactory bf) {
        bf.getButton().press();
    }
}
