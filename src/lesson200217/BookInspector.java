package lesson200217;

class Book {}
class ProgrammerBook extends Book {}

public class BookInspector {

    public static void main(String[] args) {
        Book book = new ProgrammerBook();
        ProgrammerBook progrBook = new ProgrammerBook();

        Book goodBook = progrBook;
        ProgrammerBook goodProgrBook = (ProgrammerBook) book;

        Book simpleBook = new Book();
        ProgrammerBook simpleProgrBook = (ProgrammerBook) simpleBook;//error
    }
}

