package lesson200206;

public class Increment {

    public static void main(String[] args) {
        int i = 3;
        int j = 3;

        i--;
        --j;

        System.out.println(i++);
        System.out.println(++j);

        System.out.println(i);
        System.out.println(j);

    }
}
