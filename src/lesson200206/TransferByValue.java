package lesson200206;

public class TransferByValue {

    public static void main(String[] args) {
        Storage init = new Storage(10);

        changeStorage(init);

        System.out.println(init.i);
    }

    // x = init;
    public static void changeStorage(Storage x) {
        x = new Storage(20);
        x.i = 20;
    }
}

class Storage {
    int i;

    public Storage(int i) {
        this.i = i;
    }
}
