package lesson200206;

public class IntegerCacheExample {

    public static void main(String[] args) {
        Integer x = 1000;
        System.out.println(x);

        changeInteger(x);
    }

    private static void changeInteger(Integer i) {
        System.out.println(i);

        Integer integer = 3;

        integer++;

        integer = integer + 1;

        int i1 = 128;
        int i2 = 128;

        if (i1 == i2) {

        }

        class Local {

        }
    }
}
