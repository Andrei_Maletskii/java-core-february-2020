package lesson200206;

public class InstanceOfExample {
    public static void main(String[] args) {

    }

    private static String checkNumber(Number n) {
        if (n instanceof Integer) {
            return "Integer";
        }

        if (n instanceof Double) {
            Double d = (Double) n;

            return d.toString();
        }

        return "default";
    }
}
