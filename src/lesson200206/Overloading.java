package lesson200206;

public class Overloading {

    public static void main(String[] args) {
        long lng = 1000000l;
        long lng1 = 1000000L;

        int i = Integer.MAX_VALUE;

        i++;

        System.out.println(i);
        System.out.println(Integer.MIN_VALUE);
    }
}
