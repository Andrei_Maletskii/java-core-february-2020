package lesson200206;

public class DefaultValues {
    static int ii;
    int i;
    double d;

    void method() {
        int j;

//        System.out.println(j); ERROR
        System.out.println(i);
        System.out.println(d);

//        k++; ERROR
    }

    void method2() {
        int k;
    }

    public static void main(String[] args) {
        DefaultValues instance1 = new DefaultValues();
        DefaultValues instance2 = new DefaultValues();
        instance1.i += 1;
        instance2.i += 2;
        DefaultValues.ii+=3;

        System.out.println(instance1.i);
        System.out.println(instance2.i);
        System.out.println(DefaultValues.ii);
        System.out.println(instance1.ii);
        System.out.println(instance2.ii);

    }


}
