package lesson200206;

public class Wrappers {

    public static void main(String[] args) {
        int i1 = 10;
        Integer i2 = 10;
        Integer i3 = Integer.valueOf(10);

        process(i1);
        process(i2);
        process(i2.intValue());

        Integer.valueOf(10); // boxing
        i2.intValue(); // unboxing

        process1(i2);
        process1(i1);


    }

    public static void process(int i) {

    }

    public static void process1(Integer i) {

    }
}
