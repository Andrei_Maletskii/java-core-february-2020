package lesson200206;

public class BitMove {

    public static void main(String[] args) {
        int i = Integer.MIN_VALUE;
        int j = i >>> 10; // 01000000
        int k = i >> 10; //  11000000
        int m = i << 5;
        System.out.println(j);
        System.out.println(k);
        long l = -1;
        l >>>= 10;
        System.out.println(l);
        short s = -1;
        s >>>= 10;
        System.out.println(s);
        byte b = -1;
        b >>>= 10;
        System.out.println(b);
        b = -1;
        System.out.println(b >>> 10);

    }
}
