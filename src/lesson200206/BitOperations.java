package lesson200206;

public class BitOperations {

    public static void main(String[] args) {
        System.out.println("---For positive numbers - no matter---");

        int a = 60;
        System.out.println(String.format("a  = %3d %6s", a, Integer.toBinaryString(a)));

        System.out.println("a1: a >> 1");
        int a1 = a >> 1;
        System.out.println(String.format("a1 = %3d %6s", a1, Integer.toBinaryString(a1)));

        System.out.println("a2: a >>> 1");
        int a2 = a >>> 1;
        System.out.println(String.format("a2 = %3d %6s", a2, Integer.toBinaryString(a2)));

        System.out.println("---For negative numbers---");

        int b = Integer.MIN_VALUE;

        System.out.println(String.format("b   = %11d %32s", b, Integer.toBinaryString(b)));

        System.out.println("b11: b >> 1");
        int b11 = b >> 1;
        System.out.println(String.format("b11 = %11d %32s", b11, Integer.toBinaryString(b11)));
        
        System.out.println("b12: b11 >> 1");
        int b12 = b11 >> 1;
        System.out.println(String.format("b12 = %11d %32s", b12, Integer.toBinaryString(b12)));

        System.out.println("b13: b12 >> 1");
        int b13 = b12 >> 1;
        System.out.println(String.format("b13 = %11d %32s", b13, Integer.toBinaryString(b13)));

        System.out.println("b21: b >>> 1");
        int b21 = b >>> 1;
        System.out.println(String.format("b21 = %11d %32s", b21, Integer.toBinaryString(b21)));

        System.out.println("b22: b21 >>> 1");
        int b22 = b21 >>> 1;
        System.out.println(String.format("b22 = %11d %32s", b22, Integer.toBinaryString(b22)));

        System.out.println("b23: b22 >>> 1");
        int b23 = b22 >>> 1;
        System.out.println(String.format("b23 = %11d %32s", b23, Integer.toBinaryString(b23)));
    }

}
