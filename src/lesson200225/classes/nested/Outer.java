package lesson200225.classes.nested;

public class Outer {
    private int x = 3;
    private static int y = 4;

    public void method() {
        System.out.println(Nested.value);
    }

    static class Nested {
        static int value = 5;

        public void method() {
            System.out.println("y=" + Outer.y);
            // System.out.println("x="+x);
            // ERROR
        }
    }

    public static class Nested1 {
        static int value = 5;

        public void method() {
            System.out.println("y=" + Outer.y);
            // System.out.println("x="+x);
            // ERROR
        }
    }

    public static void main(String[] args) {
        Outer outer = new Outer();
        Nested nested = new Nested();
    }
}

