package lesson200225.classes.nested;

public class Outer13 {
    private static int x = 10;

    static class Inner1 {
        public static void method() {
            System.out.println("inner1 outer.x=" + x);
        }

        public static int getOuterX() {
            return x;
        }
    }
}

