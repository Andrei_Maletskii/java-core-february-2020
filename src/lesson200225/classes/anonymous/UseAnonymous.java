package lesson200225.classes.anonymous;

public class UseAnonymous {

    public static void main(String[] args) {
        Base base = new Base() {
            {
                System.out.println(value);
                System.out.println("anon init");
            }

            @Override
            public void doWork() {
                super.doWork();
                System.out.println("anon work");
            }
        };

        base.doWork();
    }
}
