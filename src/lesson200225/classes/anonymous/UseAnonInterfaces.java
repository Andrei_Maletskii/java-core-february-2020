package lesson200225.classes.anonymous;

import java.util.Arrays;
import java.util.Comparator;

@SuppressWarnings("all")
public class UseAnonInterfaces {

    public static void main(String[] args) {
        A[] array = {new A(5), new A(6), new A(1)};

        Arrays.sort(
                array,
                new Comparator<A>() {
                    @Override
                    public int compare(A o1, A o2) {
                        Comparator<A> comparator = new Comparator<>() {

                            @Override
                            public int compare(A o1, A o2) {
                                return o1.getValue() - o2.getValue();
                            }
                        };

                        return comparator.compare(o1, o2);
                    }
                }
        );
    }
}

//class AComparator implements Comparator<A> {
//
//    @Override
//    public int compare(A o1, A o2) {
//        return 0;
//    }
//}

class A {
    private int value;

    public A(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
