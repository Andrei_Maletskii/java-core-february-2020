package lesson200225.classes.inner;

public class Example
//        extends Outer.Inner //ERROR
    extends lesson200225.classes.nested.Outer.Nested1
{
    public static void main(String[] args) throws ClassNotFoundException {
//        double pi = Outer.Inner.PI;

//        Outer.Inner inner = new Outer().new Inner(); // ERROR

        Outer outer = new Outer();
        Outer.Inner2 inner2 = outer.new Inner2();

    }
}
