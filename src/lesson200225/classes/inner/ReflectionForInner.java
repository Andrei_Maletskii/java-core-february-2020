package lesson200225.classes.inner;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

public class ReflectionForInner {

    public static void main(String[] args) throws ClassNotFoundException,
            NoSuchMethodException,
            IllegalAccessException,
            InvocationTargetException,
            InstantiationException
    {
        Class<?> outerClass = Class.forName("lesson200225.classes.inner.Outer");
//        Class<?> innerClass = Class.forName("lesson200225.classes.inner.Outer.Inner"); ERROR

        Constructor<?> outerCstr = outerClass.getDeclaredConstructor();

        Object outer = outerCstr.newInstance();

        Class<?>[] classes = outerClass.getDeclaredClasses();

        Class<?> innerClass = Arrays.stream(classes)
                .filter(clazz -> clazz.getCanonicalName()
                        .equals("lesson200225.classes.inner.Outer.Inner")
                )
                .findFirst()
                .get();

        Constructor<?> innerCstr = innerClass.getDeclaredConstructor(outerClass);

        innerCstr.setAccessible(true);

        Object inner = innerCstr.newInstance(outer);

        System.out.println(inner);
    }
}
