package lesson200225.classes.inner;

public class Outer
//        extends Outer.Inner
{


    private int value;

    public void outerMethod() {
//        innerMethod(); ERROR
//        innerValue; ERROR
    }

    public class Inner2 extends Inner {

    }

    private class Inner {
        public static final double PI = 3.14;

//        static int staticValue; ERROR

        private int innerValue;

        private int value;

        public void innerMethod() {
            Outer.this.outerMethod();
            outerMethod();
            Outer outer = Outer.this;
            System.out.println(outer.value);
        }
//        public static void staticMethod() {
//
//        }
    }


    public static void main(String[] args) {
        Outer outer = new Outer();
        Inner inner = outer.new Inner();


        double pi = Outer.Inner.PI;
    }

    private static void createInnerWithoutSavingOuter() {
        Inner inner = new Outer().new Inner();

        inner.innerMethod();

        // has reference to the instance of Outer, because of Outer.this

    }
}
