package lesson200225;

import java.lang.annotation.Annotation;

@RuntimeAnnotation
//@SuppressWarnings("all")
public class Test {

    public static void main(String[] args) throws ClassNotFoundException {
        Class<?> testClass = Class.forName("lesson200225.Test");

        Annotation[] annotations = testClass.getAnnotations();

        for (Annotation annotation : annotations) {
            System.out.println(annotation);
        }
    }
}
