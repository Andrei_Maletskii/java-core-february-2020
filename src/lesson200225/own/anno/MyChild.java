package lesson200225.own.anno;

import java.lang.annotation.Annotation;

@MyAnnotation(hello = "anotherValue")
public class MyChild extends MyParent {

    public static void main(String[] args) throws ClassNotFoundException {
        Class<?> childClass = Class.forName("lesson200225.own.anno.MyChild");
        Class<?> parentClass = Class.forName("lesson200225.own.anno.MyParent");

        MyAnnotation[] parentMyAnnotations =
                parentClass.getDeclaredAnnotationsByType(MyAnnotation.class);

        for (MyAnnotation myAnnotation : parentMyAnnotations) {
            System.out.println("parent " + myAnnotation.hello());
        }

        MyAnnotation[] childMyAnnotations =
                childClass.getAnnotationsByType(MyAnnotation.class);

        for (MyAnnotation childMyAnnotation : childMyAnnotations) {
            System.out.println("child " + childMyAnnotation.hello());
        }
    }
}
