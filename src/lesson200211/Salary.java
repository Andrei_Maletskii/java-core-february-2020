package lesson200211;

public class Salary {
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Salary salary = (Salary) o;

        return Double.compare(salary.baseSalary, baseSalary) == 0;
    }

    @Override
    public int hashCode() {
        long temp = Double.doubleToLongBits(baseSalary);
        return (int) (temp ^ (temp >>> 32));
    }

    private double baseSalary;
    public static double increaseCoeffitient = 1.5;

    public Salary(double baseSalary) {
        this.baseSalary = baseSalary;
    }

    public double calcSalary() {
        setIncreaseCoeffitient(10);
        return 0;
    }

    public static void setIncreaseCoeffitient(double newIncreaseCoeffitient) {
        increaseCoeffitient = newIncreaseCoeffitient;
//        calcSalary(); // ERROR
    }

    public static void main(String[] args) {
        Salary salary1 = new Salary(10);
        Salary salary2 = new Salary(10);
        Salary salary3 = new Salary(100);

        System.out.println(salary1.equals(salary2));

        System.out.println(salary1);
    }
}

