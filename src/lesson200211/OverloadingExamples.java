package lesson200211;

public class OverloadingExamples {

    public static void process(Number number) {
        System.out.println("number");
    }

    public static void process(Integer integer) {
        System.out.println("integer");

    }

    public static void process(Object object) {
        System.out.println("object");
    }

    public void process(int i) {

    }

    public static void main(String[] args) {
        Integer i = 10;
        process(i);
    }
}
