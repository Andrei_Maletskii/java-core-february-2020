package lesson200211;

public class Initializers {
    int i = 0;
    static int field;
    {
        i = 10;
        System.out.println("init");
    }

    static {
        int local = 10;
        field = 10;

        System.out.println("static init");
    }

    static void method() {
        int local = 10;
        System.out.println("static method");
    }


    public Initializers() {
        System.out.println("constructor");
    }

    public static void main(String[] args) {
        Initializers initializers = new Initializers();
        InitChild.method();

    }
}

class InitChild extends Initializers {
    static int i = 0;

    static {
        i = 10;
        System.out.println("static init child");
    }



    {
        System.out.println("init child");
    }

    public InitChild() {
        super();
        System.out.println("constructor child");
    }
}
