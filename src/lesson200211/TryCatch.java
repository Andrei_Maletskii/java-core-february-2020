package lesson200211;

import java.io.IOException;
import java.io.PrintStream;
import java.text.ParseException;

public class TryCatch {

    public static void main(String[] args) {
        try {
            process();
        } catch (IOException | ParseException exception) {
            exception.printStackTrace();
        }

        PrintStream out = System.out;
        out.println("hello");

        System.out.println("hello");
    }

    public static void process() throws IOException, ParseException {
        try {
            throw new RuntimeException();
        } catch (RuntimeException e) {
            if (false) {
                throw new IOException();
            }
            // NOP
        }

        throw new ParseException("", 1);
    }
}
