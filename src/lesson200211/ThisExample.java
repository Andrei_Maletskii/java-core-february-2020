package lesson200211;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ThisExample {
    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Point2D point2D = new Point2D();
        point2D.method();


//        Class<?> aClass = Class.forName("lesson200211.Point2D");
//        Method method = aClass.getDeclaredMethod("method");
//        Object result = method.invoke(point2D);
    }

}

class Point2D {
    private int x;
    private int y;

    public Point2D(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Point2D() {
//        System.out.println("hello"); error
        this(0, 0);
        System.out.println("hello");
    }

    public void method() {
        System.out.println(this);
        System.out.println("hello");
    }

    public static void method1() {
//        this; // ERROR


    }

//    public static void method(Point2D that) {
//        that.method();
//    }
}