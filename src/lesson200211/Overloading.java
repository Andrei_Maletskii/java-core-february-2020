package lesson200211;

public class Overloading {

    double max = Math.max(1.2, 1.3);

    public static String print(String s) {
        return "hello";
    }

    private static String print(String s, int i) {
        return "";
    }

    public static void main(String[] args) {
        print("Hello");

        Overloading.print("hello");
    }
}
