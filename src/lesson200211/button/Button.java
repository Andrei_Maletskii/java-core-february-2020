package lesson200211.button;

public abstract class Button {

    public abstract void press();
    public abstract void press(String s);

    public static void check() {
        System.out.println("hello");
    }
}
