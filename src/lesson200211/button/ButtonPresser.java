package lesson200211.button;

public class ButtonPresser {

    public static void main(String[] args) {
        useButton(new NuclearButton());
        useButton(new RedButton());

        Button button = new NuclearButton();

//        Button.check();
//        button.press();

        button.check();

    }

    private static void useButton(Button button) {
        button.press();
        button.press("hello");
        button.check();
    }
}
