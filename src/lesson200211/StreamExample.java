package lesson200211;

import java.util.List;
import java.util.stream.Collectors;

public class StreamExample {

    public static void main(String[] args) {
        List<Integer> integers = List.of(1, 2, 3, 4, 5);

        List<Integer> list = integers.stream()
                .sorted()
                .filter(i -> i > 2)
                .map(i -> i * i)
                .collect(Collectors.toList());

        int i,j,k; // WRONG
        int m = 10, n = 11; // WRONG

        int a;
        int b;
        int c;
        int r = 10;
        int h = 11;

//        int l;
//        for (l = 0; l < 10; l++) {
//
//        }

        for (int l = 0; l < 10; l++) {

        }
    }
}
