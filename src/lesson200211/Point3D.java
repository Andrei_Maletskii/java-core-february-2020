package lesson200211;

public class Point3D {

    private int x;
    private int y;
    private int z;

    {
        System.out.println("hello");
    }

    {
        this.initX();
    }

    public Point3D(int y, int z) {
        System.out.println("constructor");

        this.y = y;
        this.z = z;
    }

    public Point3D(int z) {
        this.z = z;
    }

    private void initX() {
        System.out.println("init x");
        this.x = 10;
    }

    public static void main(String[] args) {
        Point3D point3D = new Point3D(20, 30);

        System.out.println(point3D.x);
        System.out.println(point3D.y);
        System.out.println(point3D.z);
    }
}
