package lesson200211;

public class ReturnOfArrays {

    public static void main(String[] args) {
        int[] result = process(false);

        for (int i = 0; i < result.length; i++) {
            System.out.println(result[i]);
        }
    }

    public static int[] process(boolean b) {
        if (b) {
            return new int[] {1, 2, 3};
        }

        return null;
    }
}
